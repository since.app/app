import { trim, transform } from './rules'

export const rules = {
  title: [
    trim({ spaces: true })
  ],
  description: [
    // Description can be whatever the user wants
  ]
}

export function description (value) {
  return transform(value, rules.description)
}

export function title (value) {
  return transform(value, rules.title)
}
