// Application imports
import { transform, trim } from './rules'
import { is } from '../utils'

/**
 * Timestamp tranformation rules.
 *
 * @type {Object}
 */
export const rules = {
  title: [
    trim({ spaces: true })
  ],
  description: [
    // Description can be whatever the user wants
  ],
  date: [
    // Convert a date objects into epochs
    (value) => is.a(value, Date) ? value.getTime() : parseInt(value, 10)
  ]
}

/**
 * Timestamp date attribute transformer.
 *
 * @param   {mixed}  value Value to transform
 * @returns {mixed}        Transformed value
 */
export function date (value) {
  return transform(value, rules.date)
}

/**
 * Timestamp description attribute transformer.
 *
 * @param   {mixed}  value Value to transform
 * @returns {mixed}        Transformed value
 */
export function description (value) {
  return transform(value, rules.description)
}

/**
 * Timestamp title attribute transformer.
 *
 * @param   {mixed}  value Value to transform
 * @returns {mixed}        Transformed value
 */
export function title (value) {
  return transform(value, rules.title)
}
