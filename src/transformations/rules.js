// Application imports
import { capitalize as capitalized } from '../utils'

/**
 * Creates a capitalization transformer.
 *
 * @returns {Function} Transformer function
 */
export function capitalize () {
  /**
   * Capitalize transformer.
   *
   * @param   {String} value String to capitalize
   * @returns {String}       Capitalized string
   */
  return function transforms (value) {
    return capitalized(value)
  }
}

/**
 * Creates a lower case transformer.
 *
 * @returns {Function} Transformer function
 */
export function lower () {
  /**
   * Lower case transformer.
   *
   * @param   {String} value String to lower case
   * @returns {String}       Lower cased string
   */
  return function transforms (value) {
    return value.toLowerCase()
  }
}

/**
 * Creates a trim transformer.
 *
 * @param   {Boolean}  spaces Reduce consecutive spaces
 * @returns {Function}        Transformer function
 */
export function trim ({ spaces = false }) {
  /**
   * Trim transformer.
   *
   * @param   {String} value String to capitalize
   * @returns {String}       Capitalized string
   */
  return function transforms (value) {
    if (spaces) value = value.replace(/\s+/g, ' ')
    return value.trim()
  }
}

/**
 * Transforms a value given a set of rules.
 *
 * @param   {mixed} value Value to transform
 * @param   {Array} rules Transformation rules to run
 * @returns {mixed}       Transformed value
 */
export function transform (value, rules) {
  return rules.reduce((v, t) => t(v), value)
}
