// Application imports
import { lower, transform, trim } from './rules'

/**
 * Transformation rules for authentication.
 *
 * @type {Object}
 */
export const rules = {
  nickname: [
    trim({ spaces: true })
  ],
  username: [
    lower(),
    trim({ spaces: true })
  ],
  password: [
    // Don't transform passwords
  ]
}

/**
 * User nickname transformer.
 *
 * @function           nickname
 * @param    {String}  value    Value to transform
 * @returns  {String}           Transformed value
 */
export function nickname (value) {
  return transform(value, rules.nickname)
}

/**
 * User password transformer.
 *
 * @function           password
 * @param    {String}  value    Value to transform
 * @returns  {String}           Transformed value
 */
export function password (value) {
  return transform(value, rules.password)
}

/**
 * User username transformer.
 *
 * @function           username
 * @param    {String}  value    Value to transform
 * @returns  {String}           Transformed value
 */
export function username (value) {
  return transform(value, rules.username)
}
