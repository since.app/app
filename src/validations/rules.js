/**
 * Creates an email validator.
 *
 * @returns {Function} Validator function
 */
export function email () {
  /**
   * Email value validator.
   *
   * @param   {mixed}       value Value to validate
   * @returns {String|void}       Error message if invalid
   */
  return function validates (value) {
    if (!/^\S+@\S{2,}.\S{2,}/.test(value)) return 'is not a valid email address'
  }
}

/**
 * Creates a length validator.
 *
 * @param   {Number}   min Minimum length allowed(0 for no limit)
 * @param   {Number}   max Maximum length allowed(0 for no limit)
 * @returns {Function}     Validator function
 */
export function length ({ min = 0, max = 0 }) {
  /**
   * Length value validator.
   *
   * @param   {mixed}       value Value to validate
   * @returns {String|void}       Error message if invalid
   */
  return function validates (value) {
    if (!Object.hasOwnProperty.call(value, 'length')) return
    if (min && value.length < min) return `must contain at least ${min} characters`
    if (max && value.length > max) return `must not contain more than ${max} characters`
  }
}

/**
 * Creates a required validator.
 *
 * @returns {Function} Validator function
 */
export function required () {
  /**
   * Required value validator.
   *
   * @param   {mixed}       value Value to validate
   * @returns {String|void}       Error message if invalid
   */
  return function validates (value) {
    if ([undefined, null, ''].includes(value)) return 'cannot be blank'
  }
}

/**
 * Validates a value against a given set of rules.
 *
 * @param   {mixed}       value Value to validate
 * @param   {Array}       rules Validation rules to run
 * @returns {String|null}       Error message if invalid
 */
export function validate (value, rules) {
  const error = rules.find((validator) => validator(value))
  return error ? error(value) : null
}
