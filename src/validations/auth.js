// Application imports
import { email, length, validate } from './rules'

/**
 * Validation rules for authentication.
 *
 * @type {Object}
 */
export const rules = {
  nickname: [
    length({ min: 3, max: 32 })
  ],
  username: [
    email(),
    length({ max: 32 })
  ],
  password: [
    length({ min: 8, max: 32 })
  ]
}

/**
 * User nickname attribute validator.
 *
 * @function           nickname
 * @param    {String}  value    Value to validate
 * @returns  {Boolean}          Value is valid
 */
export function nickname (value) {
  return validate(value, rules.nickname)
}

/**
 * User password attribute validator.
 *
 * @function           password
 * @param    {String}  value    Value to validate
 * @returns  {Boolean}          Value is valid
 */
export function password (value) {
  return validate(value, rules.password)
}

/**
 * User username attribute validator.
 *
 * @function           username
 * @param    {String}  value    Value to validate
 * @returns  {Boolean}          Value is valid
 */
export function username (value) {
  return validate(value, rules.username)
}
