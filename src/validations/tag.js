// Application imports
import { length, required, validate } from './rules'

/**
 * Validation rules for each tag input.
 *
 * @type {Object}
 */
export const rules = {
  title: [
    required(),
    length({ max: 80 })
  ],
  description: [
    length({ max: 800 })
  ]
}

/**
 * Timestamp description attribute validator.
 *
 * @param   {String}  value Value to validate
 * @returns {Boolean}       Value is valid
 */
export function description (value) {
  return validate(value, rules.description)
}

/**
 * Timestamp title attribute validator.
 *
 * @param   {String}  value Value to validate
 * @returns {Boolean}       Value is valid
 */
export function title (value) {
  return validate(value, rules.title)
}
