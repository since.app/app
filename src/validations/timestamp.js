// Application imports
import { length, required, validate } from './rules'
import { is } from '--/utils'

/**
 * Validation rules for timestamp input.
 *
 * @type {Object}
 */
export const rules = {
  title: [
    required(),
    length({ max: 80 })
  ],
  description: [
    length({ max: 800 })
  ],
  date: [
    // Make sure the date was parsed by the transformer
    (value) => is.a(value, Number)
  ]
}

/**
 * Timestamp date attribute validator.
 *
 * @param   {String}  value Value to validate
 * @returns {Boolean}       Value is valid
 */
export function date (value) {
  return validate(value, rules.date)
}

/**
 * Timestamp description attribute validator.
 *
 * @param   {String}  value Value to validate
 * @returns {Boolean}       Value is valid
 */
export function description (value) {
  return validate(value, rules.description)
}

/**
 * Timestamp title attribute validator.
 *
 * @param   {String}  value Value to validate
 * @returns {Boolean}       Value is valid
 */
export function title (value) {
  return validate(value, rules.title)
}
