// Package imports
import { Show } from 'solid-js/dom'

// Application imports
import { use as data } from './store/data'
import { use as auth } from './store/auth'
import Auth from './components/auth'
import Disclaimer from './components/auth/disclaimer'
import Chronological from './components/data/chronological'
import Clock from './components/clock'
import Create from './forms/timestamp/create'
import Debug from './components/data/debug'
import Menu from './components/auth/menu'

// Compile CSS
import './App.css'

export default function App () {
  const [user, { persist, state }] = auth()
  const [store] = data()

  const { views: { is: view } } = state()

  return (
    <>
      <div class='mx-auto sm:container mb-20'>
        <Disclaimer />

        <Show when={view.closed()}>
          <Clock />

          <Create />

          <Chronological payload={store.timestamps} />

          <Debug visible={persist().debug} payload={store} />
        </Show>

        <Show when={!view.closed()}>
          <Menu />

          <Auth />

          <Debug visible={persist().debug} payload={user} />
        </Show>
      </div>
    </>
  )
}
