export const tr = {
  addSince: 'Ajouter',
  confirmDelete: 'Est-ce que vous voulez supprimer l\'item?',
  dateFormat: 'l, F jS h:i:sa',
  noDescription: 'Pas de description',
  since: 'Depuis'
}

/**
 * Returns the French name for the day of the week.
 *
 * @params  {Date}   date Date to label
 * @returns {String}
 */
export function day (date) {
  return ([
    'dimanche',
    'lundi',
    'mardi',
    'mercredi',
    'jeudi',
    'vendredi',
    'samedi'
  ])[date.getDay()]
}

/**
 * Returns the French month name.
 *
 * @params  {Date}   date Date to label
 * @returns {String}
 */
export function month (date) {
  return ([
    'janvier',
    'février',
    'mars',
    'avril',
    'mai',
    'juin',
    'juillet',
    'août',
    'septembre',
    'octobre',
    'novembre',
    'decembre'
  ])[date.getMonth()]
}

/**
 * Returns the suffix for day numbers.
 *
 * Getting the suffix for a number is a bit trickier than accessing an array index.
 *
 * @params  {Date}   date Date to label
 * @returns {String}
 */
export function suffix (date) {
  return date.getDate() === 1 ? 'er' : ''
}
