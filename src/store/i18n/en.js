export const tr = {
  auth: {
    login: {
      password: 'Password',
      register: 'Need an account?',
      submit: 'Login',
      username: 'Email'
    },
    register: {
      login: 'Log in',
      nickname: 'Username',
      password: 'Password',
      submit: 'Register',
      username: 'Email'
    },
    user: {
      logout: 'Log out',
      welcome: 'Hello'
    }
  },
  clock: {
    format: 'l, F jS h:i:sa'
  },
  persist: {
    local: {
      error: 'error',
      name: 'Browser local storage',
      offline: 'not accessible',
      synced: 'sync success',
      syncing: 'sync in progress'
    },
    remote: {
      error: 'error',
      name: 'Since.app remote database',
      offline: 'not connected',
      synced: 'sync success',
      syncing: 'sync in progress'
    }
  },
  timestamp: {
    confirm: {
      delete: 'Do you want to delete this item?',
      discard: 'Discard unsaved changes?'
    },
    create: 'Add Since',
    description: 'Description',
    descriptionless: 'No description',
    since: 'Since',
    title: 'Title'
  }
}

/**
 * Returns the English name for the day of the week.
 *
 * @params  {Date}   date Date to label
 * @returns {String}
 */
export function day (date) {
  return ([
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ])[date.getDay()]
}

/**
 * Returns the English month name.
 *
 * @params  {Date}   date Date to label
 * @returns {String}
 */
export function month (date) {
  return ([
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ])[date.getMonth()]
}

/**
 * Returns the suffix for day numbers.
 *
 * Getting the suffix for a number is a bit trickier than accessing an array index.
 *
 * @params  {Date}   date Date to label
 * @returns {String}
 */
export function suffix (date) {
  const day = date.getDate()

  // Teens are always suffixed with "th" so 10th, 11th, 12th, 13th, etc.
  if (day >= 10 && day <= 20) return 'th'

  // 1st, 2nd, 3rd, 4th, 5th, 6th, 7th, etc.
  return (['th', 'st', 'nd', 'rd'])[day % 10] || 'th'
}
