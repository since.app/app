// Package imports
import { createComputed, createContext, createMemo, createState, on, untrack, useContext } from 'solid-js'

// Application imports
import persistence from './data/persist'
import { clone, expunge, transpose } from '--/utils'
import { inject as injectTimestamp } from './data/timestamp'
import { inject as injectState } from './data/state'
import { use as auth } from './auth'

// Exporting shortcuts to deep functions
export { createSetters, getGraphError } from '--/store/drivers/lokka'

// Data store context
const DataContext = createContext()

/**
 * Helper function that returns a data map with only timestamp models.
 *
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export function filterTimestamps (data) {
  return expunge(data, (item) => !item.date)
}

/**
 * Helper function that returns a data map with only tag models.
 *
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export function filterTags (data) {
  return expunge(data, (item) => !!item.date)
}

/**
 * Data provider.
 *
 * @function         Provider
 * @param   {Object} props    Provider properties
 * @returns {JSX}
 */
export function Provider (props) {
  const [{ stored: persisted, synced: persists }, { local, remote }] = persistence('since.data')
  const [, { guest }] = auth()

  // Create timestamp store
  const [timestamps, setTimestamps] = createState()

  // Create our full data store
  const [state] = createState({ persists, timestamps })

  // Creating getters from memos that only react to changes
  const getTimestamps = createMemo(() => clone(state.timestamps), {}, true)

  // Create our public actions object
  const actions = {}

  // Helper flags
  const skip = {
    // Flag to ignore state updates while synchronizing
    sync: false,
    // Same for timestamps otherwise it would delete everything
    timestamps: true
  }

  // Load data from our persistence
  const syncPersisted = (data) => {
    // Prevent computation on state.timestamps when synchronizing data stores
    skip.sync = true

    // Does not trigger our persistence push functions
    setTimestamps(filterTimestamps(data))

    // Resume tracking changes
    skip.sync = false
  }

  // Fired in a computed(on(guest)) signal so no need to untrack data
  const syncRemote = (data) => {
    // Create an object containing all models
    const current = state.timestamps

    // Using tranpose to replace old remote data with updated state data
    const payload = transpose(data, (item, id) => {
      return current[id] && current[id].updated > item.updated ? current[id] : item
    })

    // Persisting all changes to state
    syncPersisted(payload)

    // Save changes to local storage
    local.sync(state.timestamps)

    // Save changes to remote database
    remote.sync(state.timestamps)
  }

  // Inject timestamp management actions
  /** @todo refactor injectTimestamp to match auth's paradigm */
  injectTimestamp(actions, setTimestamps)

  // Inject state management actions
  injectState(actions)

  // Computation on state.timestamps
  createComputed(on(getTimestamps, () => {
    // Ignore sync events and our initial empty state
    if (skip.sync || skip.timestamps) {
      skip.timestamps = false
    } else {
      // Always try to save data locally
      local.push(getTimestamps())

      // Only push data to remote if user is connected
      if (!guest()) remote.push(getTimestamps())
    }
  }))

  // Computation on mount
  createComputed(async () => {
    const pull = await local.pull()

    if (pull === true) {
      syncPersisted(untrack(() => persisted.local))
    } else {
      console.error(pull)
    }
  })

  // Computation on guest signal
  createComputed(on(guest, async () => {
    if (guest()) return remote.flush()
    const pull = await remote.pull()

    if (pull === true) {
      syncRemote(persisted.remote)
    } else {
      console.error(pull)
    }
  }))

  // Creating final returned context object
  const data = [state, actions]

  // Return the context wrapped child DOM
  return (
    <DataContext.Provider value={data}>
      {props.children}
    </DataContext.Provider>
  )
}

/**
 * Returns the data context.
 *
 * @function         use
 * @returns {Object}     Resolved data provider value
 */
export function use () {
  return useContext(DataContext)
}
