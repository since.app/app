// Application imports
import { Provider as AuthProvider } from './auth'
import { Provider as DataProvider } from './data'
import { Provider as I18nProvider } from './i18n'
import { Provider as SyncProvider } from './sync'

/**
 * Store provider that wraps children with all our providers.
 *
 * @function          Provider
 * @param    {Object} props    Provider properties
 * @returns  {JSX}
 */
export function Provider (props) {
  return (
    <I18nProvider>
      <SyncProvider>
        <AuthProvider>
          <DataProvider>
            {props.children}
          </DataProvider>
        </AuthProvider>
      </SyncProvider>
    </I18nProvider>
  )
}
