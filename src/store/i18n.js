// Package imports
import { createContext, createResourceState, createSignal, createState, useContext } from 'solid-js'

// Application imports
import { is, phpdate } from '--/utils'
import * as en from './i18n/en'
import * as fr from './i18n/fr'

const languages = { en, fr }

/**
 * Internationalization service context.
 *
 * @type {Context}
 */
const I18nContext = createContext()

/**
 * Creates our internationalization service wrapper.
 *
 * @function          Provider
 * @param    {Object} props    Component properties
 * @returns  {JSX}             Service wrapper
 */
export function Provider (props) {
  const [language, setLanguage] = createSignal('en')
  const [translations, setTranslations] = createState({ ...en.tr })
  const [state, loadState] = createResourceState({ en })

  // Translation actions
  const actions = {
    format (date) {
      return phpdate(is.a(date, Date) ? date : new Date(date), translations.clock.format, actions)
    },
    getLanguages () {
      return Object.keys(languages)
    },
    setLanguage (to) {
      if (state[to]) {
        setLanguage(to)
        setTranslations({ ...state[to].tr })
      } else {
        /** @todo implement fetching other languages */
        loadState({ [to]: () => languages[to] })
        actions.setLanguage(to)
      }
    },
    day (date) {
      return state[language()].day(date)
    },
    month (date) {
      return state[language()].month(date)
    },
    suffix (date) {
      return state[language()].suffix(date)
    }
  }

  // Build final store
  const i18n = [translations, actions]

  return (
    <I18nContext.Provider value={i18n}>
      {props.children}
    </I18nContext.Provider>
  )
}

/**
 * Internationalization service injector.
 *
 * @function            use
 * @returns  {Function}     Service invoker
 */
export function use () {
  return useContext(I18nContext)
}
