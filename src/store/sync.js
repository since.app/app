// Package imports
import { createContext, createSignal, onCleanup, useContext } from 'solid-js'

// Create our context container
const SyncContext = createContext()

/**
 * Sync provider.
 *
 * @function          Provider
 * @param    {Object} props    Provider properties
 * @returns  {JSX}
 */
export function Provider (props) {
  /**
   * Delta date object.
   *
   * @type {Date}
   */
  let date = new Date()

  // Create signals for a few different time intervals
  const [seconds, setSeconds] = createSignal(date)
  const [minutes, setMinutes] = createSignal(date)

  // Create our decisecond clock
  const id = setInterval(() => {
    // Get the current date
    const now = new Date()

    // Updating the minutes signal only once per minute
    if (date.getMinutes() !== now.getMinutes()) setMinutes(now)

    // Fires every second and updates the previous date object
    setSeconds(date = now)
  }, 1000)

  // Clear our interval in case it ever gets garbage collected
  onCleanup(() => clearInterval(id))

  // Export the various signal getters
  const sync = [{ seconds, minutes }]

  // Return the context wrapped child DOM
  return (
    <SyncContext.Provider value={sync}>
      {props.children}
    </SyncContext.Provider>
  )
}

/**
 * Returns the synchronization context.
 *
 * @function         use
 * @returns {Object}     Resolved sync provider value
 */
export function use () {
  return useContext(SyncContext)
}
