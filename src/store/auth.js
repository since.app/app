// Package imports
import { createComputed, createContext, createMemo, on, useContext } from 'solid-js'

// Application imports
import lokka from '--/api/auth'
import { make as createState, createSetters as stateSetters } from '--/store/drivers/state'
import { inject as injectToken } from './auth/token'
import { inject as injectUser } from './auth/user'
import { inject as injectState } from './auth/state'
import { is } from '--/utils'

// Auth uses Lokka as driver so use its error handler to get GraphQL errors
export { getGraphError } from '--/store/drivers/lokka'

/**
 * Auth store context.
 *
 * @type {Context}
 */
const AuthContext = createContext()

/**
 * Location hash identifying requests to validate email addresses.
 *
 * @type {String}
 */
const VERIFY_EMAIL = '#v.'

/**
 * Creates shortcut sync state and status setters.
 *
 * @param   {Function} setStored State setter
 * @param   {Function} setSynced Status setter
 * @returns {Object}             Mapped shortcut functions
 */
export function createSetters (setStored, setSynced) {
  const { store, sync } = stateSetters(setStored, setSynced)

  return {
    store,
    sync,
    auth (status) {
      setSynced({ token: status, user: status })
    }
  }
}

/**
 * Auth provider.
 *
 * @function          Provider
 * @param    {Object} props    Provider properties
 * @returns  {JSX}
 */
export function Provider (props) {
  // Create our authentication state
  const [{ stored }, { flag: setSynced, push: setStored }] = createState({
    // Auth token server response
    token: {},
    // Authenticated user data
    user: {}
  })

  // Authentication state
  const guest = createMemo(() => is.null(() => stored.token.bearer), true, true)

  // Public authentication actions
  const actions = { guest }

  // Creating our GraphQL client
  const client = lokka()

  // Flag to skip initial guest() signal
  let loading = true

  // Inject user actions(register, update)
  injectUser(client, actions, setStored, setSynced)

  // Inject JWT actions(login, logout, refresh)
  injectToken(client, actions, setStored, setSynced)

  // Inject state management actions
  injectState(actions)

  // Shortcut to the authentication localStorage persistence
  const { persist, state } = actions

  // Computation on guest
  createComputed(on(guest, () => {
    // User logged in, store token locally so it persists between refreshes
    if (!loading && !guest()) persist({ token: stored.token.token })

    // User logged out so delete the offline token
    if (!loading && guest()) persist({ token: undefined })

    // Only skip the initial computation
    loading = false
  }))

  // Computation on stored.errors
  createComputed(() => {
    // Clear the localStorage token if there were any errors authenticating it
    if (stored.errors.token) persist({ token: undefined })
  })

  // Computation on mount
  createComputed(async () => {
    // Attempt to validate an existing JWT from localStorage
    if (persist().token) actions.validate({ token: persist().token })

    // We only manipulate the location once so no need to make a router
    const hash = window.location.hash

    // Registration is still validated client side but through the URL
    if (hash.slice(0, VERIFY_EMAIL.length) === VERIFY_EMAIL) {
      // Switch to the veryfing registration view
      state().views.verify()

      // Attempt to validate user registration
      await actions.email({ token: hash.slice(VERIFY_EMAIL.length) })

      // Remove the token from the hash to prevent re-attempts on refresh
      window.location.hash = ''
    }
  })

  // Create our store provider
  const auth = [stored, actions]

  return (
    <AuthContext.Provider value={auth}>
      {props.children}
    </AuthContext.Provider>
  )
}

/**
 * Auth service accessor.
 *
 * @return {Object} Auth provider value
 */
export function use () {
  return useContext(AuthContext)
}
