// Package imports
import { batch, createComputed, createState, untrack } from 'solid-js'

// Application imports
import { clone, delta, expunge, merge, is, iterate, transpose, zip } from '--/utils'

/**
 * Creates shortcut sync state and status setters.
 *
 * @function            createSetters
 * @param    {Function} setStored     State setter
 * @param    {Function} setSynced     Status setter
 * @returns  {Object}                 Mapped shortcut functions
 */
export function createSetters (setStored, setSynced) {
  return {
    store (data) {
      setStored(data)
      setSynced(transpose(data, (value) => is.undef(value) ? undefined : true))
    },
    sync (data) {
      setSynced(data)
    }
  }
}

/**
 * Updates a state with the provided deltas.
 *
 * @function           fastforward
 * @param    {Object}  state        State to fast-forward
 * @param    {Object}  deltas       Deltas to apply to state
 * @param    {Boolean} [prune=true] Apply delete deltas
 */
export function fastforward (state, deltas, prune = true) {
  // Returns model UUIDs or object keys changed
  const keys = (sources) => sources.map((item) => item.id || Object.keys(item)).flat()

  // Returns models as UUID payloads if applicable
  const values = (sources) => sources.map((item) => item.id ? { [item.id]: item } : item)

  // Remove deleted data
  if (prune) expunge(state, zip(keys(deltas.deleted), true), false)

  // Append and update any existing data
  merge(state, [values(deltas.created), values(deltas.updated)].flat(), false)
}

/**
 * Flags a payload's sync status.
 *
 * @function            flagState
 * @param    {Object}   payload   Payload to flag
 * @param    {Function} setSynced Sync status setter
 */
export function flagState (payload, setSynced) {
  iterate(payload, setSynced, true)
}

/**
 * Removes all stored data.
 *
 * @function            flushState
 * @param    {Object}   stored     State to flush
 * @param    {Function} setStored  Stored state setter
 * @param    {Function} setSynced  Synced state setter
 */
export function flushState (stored, synced, setStored, setSynced) {
  // DRY'ing up flushing stored and synced state data
  const flush = (key) => setStored({ [key]: undefined }) || setSynced({ [key]: undefined })

  // Flush all stored data
  iterate(stored, flush, true)

  // Remote data can result in an empty stored state with error messages populating the synced state
  iterate(synced, flush, true)
}

/**
 * Create a persistence state manager.
 *
 * @function          make
 * @param    {Object} init Initialisation data
 * @returns  {Array}       State getters and actions
 */
export function make (init = {}) {
  // Prevent user from creating meta props
  if (init.errors) throw new Error('Driver reserves the state key "errors"')
  if (init.loading) throw new Error('Driver reserves the state key "loading"')

  // Create the storage state
  const [stored, setStored] = createState(init)

  // Create the synced state and set all initialized values as synchronized
  const [synced, setSynced] = createState(zip(Object.keys(init), true))

  // Resource meta states
  const [loading, setLoading] = createState()
  const [errors, setErrors] = createState()

  // Assign meta props to state without making them iterable
  Object.defineProperties(stored, {
    errors: { value: errors },
    loading: { value: loading }
  })

  // Update meta states
  createComputed(() => {
    iterate(synced, (value, key) => {
      // Meta values to set
      const isError = is.str(value) ? value : false
      const isLoading = value === false

      // Existing meta values
      const keyErrors = untrack(() => errors[key])
      const keyLoading = untrack(() => loading[key])

      // Update meta values if they differ
      if (keyErrors !== isError) setErrors(key, isError)
      if (keyLoading !== isLoading) setLoading(key, isLoading)
    })
  })

  return [
    { stored, synced },
    {
      flag: (payload) => flagState(payload, setSynced),
      flush: () => flushState(stored, synced, setStored, setSynced),
      push: (payload) => pushState(payload, setStored, setSynced),
      sync: (payload, persist) => syncState(payload, persist, stored, setStored, setSynced)
    }
  ]
}

/**
 * Loads an item into our state and sets its synced as synchronized.
 *
 * @function            pushState
 * @param    {Object}   payload   Payload to set as stored and synced
 * @param    {Function} setStored Stored state setter
 * @param    {Function} setSynced Synced state setter
 */
export function pushState (payload, setStored, setSynced) {
  iterate(payload, (item, key) => {
    // Batch to prevent deleting objects from triggering updates
    batch(() => {
      // Need to delete object keys before we can set empty objects
      if (is.obj(item) && is.empty(item)) setStored(key, undefined)

      // Update the state data
      setStored(key, item)

      // Update the state synced
      setSynced(key, is.undef(item) ? undefined : true)
    })
  })
}

/**
 * Synchronize persistence function.
 *
 * @param  {Object} payload Data to synchronize with persistence
 * @param  {Object|Function} persist Persistence function(s)
 * @return {[type]}         [description]
 */
export function syncState (payload, persist, state, setStored, setSynced) {
  // Get the differences between the payload and our current state
  const deltas = delta(state, payload)

  // Make sure there are changes to commit
  if (is.empty(deltas)) return

  // We need to convert our deltas into another format
  const commits = { committed: [], created: [], deleted: [], updated: [] }

  // Quickly identify changes
  iterate(deltas, (key) => {
    const uuid = is.uuid(key)
    const source = clone(state[key])
    const target = clone(payload[key])
    const deletes = is.undef(target)
    const raw = { [key]: deletes ? source : target }

    // Flagging items as being synchronized
    setSynced(key, false)

    // Pushing all changes to an easily mapped array
    commits.committed.push(uuid ? deletes ? source : target : raw)

    if (deletes) {
      // Paylods can be an object of IDs with undefined values to delete them
      commits.deleted.push(uuid ? source : raw)
    } else if (is.undef(source)) {
      // No related state object found so create it
      commits.created.push(uuid ? target : raw)
    } else {
      // Some difference was found in the payload
      commits.updated.push(uuid ? source.updated > target.updated ? source : target : raw)
    }
  }, true)

  // Checking if we have a commit function or an object of functions
  if (is.fn(persist)) {
    iterate(persist(commits, clone(state)), ({ key, value, synced }) => {
      // Commits a key and value to our state
      batch(() => {
        // Same as pushState except we allow setting the synced status to something other than true
        if (is.obj(value) && is.empty(value)) setStored(key, undefined)

        setStored(key, value)
        setSynced(key, is.undef(value) ? undefined : synced)
      })
    })
  } else if (is.obj(persist, true)) {
    // DRY'ing up checking and calling commit functions
    const has = (key) => is.len(commits[key]) && is.fn(persist[key])
    const run = (key) => persist[key](commits[key], setStored, setSynced)

    if (has('created')) run('created')
    if (has('deleted')) run('deleted')
    if (has('updated')) run('updated')
  } else {
    throw new TypeError('Driver requires "persist" to be a function or an object of functions')
  }
}
