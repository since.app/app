// Application imports
import { is, iterate } from '--/utils'
import { createSetters as stateSetters, make as createState } from './state'
import * as timestamp from '--/store/data/persist/lokka/timestamp'

// DRY function that attempts to return an instance of Lokka that's connected
const engine = (lokka) => {
  if (!lokka.client()) lokka.connect()
  if (!lokka.client()) throw new Error('Lokka not connected')
  return lokka.client()
}

/**
 * Overloads our state setters to handle UUID based model data.
 *
 * @function                createSetters
 * @param    {String|Array} uuid          Model UUID(s)
 * @param    {Function}     setStored     Stored state setter
 * @param    {Function}     setSynced     Synced state setter
 * @returns  {Object}                     Overloaded setters
 */
export function createSetters (data, setStored, setSynced) {
  if (!is.arr(data)) data = [data]

  const { store, sync } = stateSetters(setStored, setSynced)

  // Helper function that returns the value to set from a given payload for a given model
  const related = (payload, item, i) => is.arr(payload)
    ? payload[i]
    : is.obj(payload) && is.key(payload, item.id)
      ? payload[item.id]
      : payload

  return {
    store (payload) {
      iterate(data, (item, i) => {
        store({ [item.id]: related(payload, item, i) })
        sync({ [item.id]: is.undef(related(payload, item, i)) ? undefined : true })
      })
    },
    sync (payload) {
      iterate(data, (item, i) => {
        sync({ [item.id]: related(payload, item, i) })
      })
    }
  }
}

/**
 * Disconnects our Lokka instance and clears our remote sync state.
 *
 * @function            flushStored
 * @param    {Object}   lokka       Lokka connection interface
 * @param    {Function} flush       State flush function
 */
export function flushState (lokka, flush) {
  // Clears any JWT refresh timeouts
  lokka.disconnect()

  // Flush state data
  flush()
}

/**
 * Returns the first GraphQL error or throws the original if none exists.
 *
 * @function          getGraphError
 * @param    {Error}  error         Error thrown
 * @returns  {String}               GraphQL error message
 * @throws   {Error}                Non-GraphQL error
 */
export function getGraphError (error) {
  // Don't catch non-GraphQL errors
  if (!error.rawError) throw error

  // We only display one error message so get the first one
  return error.rawError[0].message
}

/**
 * Creates our Lokka persistence driver.
 *
 * @function          make
 * @param    {Object} lokka Lokka connection interface
 * @returns  {Array}        Returns the state and action tuple
 */
export function make (lokka) {
  const [{ stored, synced }, { flush, push, sync }] = createState()

  return [
    { stored, synced },
    {
      flush: () => flushState(lokka, flush),
      pull: () => pullState(lokka, push),
      push: (payload, persist) => pushState(lokka, payload, persist, sync),
      sync: (payload, persist) => syncState(lokka, payload, persist, sync)
    }
  ]
}

/**
 * Attempts to retrieve data that was persisted remotely.
 *
 * @function               pullStored
 * @param    {Object}      lokka      Lokka connection interface
 * @param    {Function}    push       State and sync status setter
 * @returns  {true|String}            True on success or the first GraphQL error
 * @throws   {Error}                  Throws non-GraphQL errors
 */
export async function pullState (lokka, push) {
  const pushes = (item) => push({ [item.id]: item })
  let results = true

  try {
    const db = engine(lokka)
    const timestamps = await timestamp.queryTimestamps(db)

    iterate(timestamps, pushes)
  } catch (e) {
    results = getGraphError(e)
  }

  return results
}

/**
 * Attempts to persist changes between the payload and the previous state.
 *
 * @function               pushStored
 * @param    {Object}      lokka      Lokka connection interface
 * @param    {Object}      payload    Full state to persist
 * @param    {Object}      persist    Object of persistence functions that creates/updates/deletes
 * @param    {Function}    sync       Sync function that run handles deltas
 * @returns  {true|String}            True on success or the first GraphQL error
 * @throws   {Error}                  Throws non-GraphQL errors
 */
export async function pushState (lokka, payload, persist, sync) {
  let results = true

  try {
    const db = engine(lokka)

    sync(payload, {
      created () {
        persist.created(db, ...arguments)
      },
      deleted () {
        persist.deleted(db, ...arguments)
      },
      updated () {
        persist.updated(db, ...arguments)
      }
    })
  } catch (e) {
    results = getGraphError(e)
  }

  return results
}

/**
 * Attempts to persist changes between the payload and the previous state without deleting anything.
 *
 * @function               syncStored
 * @param    {Object}      lokka      Lokka connection interface
 * @param    {Object}      payload    Full state to persist
 * @param    {Object}      persist    Object of persistence functions that creates/updates/deletes
 * @param    {Function}    sync       Sync function that run handles deltas
 * @returns  {true|String}            True on success or the first GraphQL error
 * @throws   {Error}                  Throws non-GraphQL errors
 */
export async function syncState (lokka, payload, persist, sync) {
  let results = true

  try {
    const db = engine(lokka)

    sync(payload, {
      created () {
        persist.created(db, ...arguments)
      },
      updated () {
        persist.updated(db, ...arguments)
      }
    })
  } catch (e) {
    results = getGraphError(e)
  }

  return results
}
