// Application imports
import { make as createState, fastforward } from './state'
import { iterate } from '--/utils'

/**
 * Attempts to return a usable window.localStorage engine.
 *
 * @returns {Storage} Usable engine
 * @throws  {Error}   Invalid engine
 */
export function engine () {
  const storage = window.localStorage
  if (!storage) throw new Error('Cannot access storage engine')
  return storage
}

/**
 * Gets a parsed window.localStorage key value.
 *
 * @param   {String}     key Key to return
 * @returns {Array|null}     Stored data or null
 */
export function pullStorage (key) {
  // Load data from storage engine
  return JSON.parse(engine().getItem(key) || null)
}

/**
 * Attempts to pull items from our localStorage persistence.
 *
 * @param   {String}         key    Storage key to use
 * @param   {Function}       stored  Stored data stored setter
 * @param   {Function}       synced Sync synced stored setter
 * @returns {Boolean|String}        Sync success or thrown error message
 */
export function pullStored (key, push) {
  let results = true

  try {
    // Load data from window.localStorage and commit it to our stored
    iterate(pullStorage(key) || [], (item, key) => push({ [item.id || key]: item }))
  } catch (e) {
    // Update return message
    results = e.message
  }

  return results
}

/**
 * Sets a window.localStorage key value.
 *
 * @param  {String} key  Key to set
 * @param  {mixed} data Data to set
 */
export function pushStorage (key, payload) {
  // Convert UUID stored map into just the stored values
  const flat = Object.values(payload)

  // Checks to see if all stored data consists of object models with "id" attributes
  const models = flat.every((item) => item.id)

  // Write data to storage engine
  engine().setItem(key, JSON.stringify(flat.length && models ? flat : payload))
}

/**
 * Attempts to push an item into our localStorage persistence.
 *
 * @param   {mixed}          data  Storage data to push
 * @param   {String}         key   Storage key to use
 * @param   {Function}       store Stored data stored setter
 * @param   {Function}       sync  Sync synced stored setter
 * @returns {Boolean|String}       Sync success or thrown error message
 */
export function pushStored (key, payload, sync) {
  let results = true

  try {
    // We get a copy of the previous stored that we'll update to commit locally
    sync(payload, (deltas, state) => {
      // Create our final stored to commit to storage
      fastforward(state, deltas)

      // Commit data to localStorage
      pushStorage(key, state)

      // Flattening since we return an array of object keys
      return deltas.committed.map((item) => {
        const deleted = deltas.deleted.includes(item)

        // Return an updated or deleted model
        if (item.id) return { key: item.id, value: deleted ? undefined : item, synced: true }

        // Convert objects into explicit keys
        return Object.keys(item).map((key) => {
          return { key, value: deleted ? undefined : item[key], synced: true }
        })
      }).flat()
    })
  } catch (e) {
    results = e.message
  }

  return results
}

export function syncStored (key, payload, sync) {
  let results = true

  try {
    sync(payload, (deltas, state) => {
      // Create our final stored to commit to storage without deleting anything
      fastforward(state, deltas, false)

      // Commit data to localStorage
      pushStorage(key, state)

      return deltas.committed.map((item) => {
        if (item.id) return { key: item.id, value: item, synced: true }
        return Object.keys(item).map((key) => {
          return { key, value: item[key], synced: true }
        })
      }).flat()
    })
  } catch (e) {
    results = e.message
  }

  return results
}

/**
 * Creates a new localStorage persistence.
 *
 * @param   {String} key Storage key to hold data in
 * @returns {Object}     Storage actions
 */
export function make (key) {
  // Keep track of the synced and what's stored in our local storage
  const [{ stored, synced }, { flush, push, sync }] = createState()

  // Expose synced, stored data, and actions to manage data
  return [
    { stored, synced },
    {
      flush,
      pull: () => pullStored(key, push),
      push: (payload) => pushStored(key, payload, sync),
      sync: (payload) => syncStored(key, payload, sync)
    }
  ]
}
