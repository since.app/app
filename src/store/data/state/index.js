// Package imports
import { createState } from 'solid-js'

// Application imports
import * as timestamps from './timestamp'

/**
 * Injects state actions.
 *
 * @function          inject
 * @param    {Object} actions Actions object to populate
 */
export function inject (actions) {
  const [getState, setState] = createState()

  Object.assign(actions, {
    state: (timestamp) => state(timestamp, getState, setState, actions)
  })
}

/**
 * Creates, caches, and returns a state manager for the given timestamp.
 *
 * @function            state
 * @param    {Object}   timestamp Timestamp to manage
 * @param    {Object}   state     Volatile state object
 * @param    {Function} setState  Volatile state setter
 * @param    {Object}   actions   List of actions available to state handlers
 * @returns  {Object}             Reference to the timestamp's state
 */
export function state (timestamp, state, setState, actions) {
  // Helper function that gets the given timestamp's state
  /** @todo investigate/implement garbage cleanup when timestamps are deleted */
  const reference = () => state[timestamp.id]

  // Helper function that sets the given timestamp's state
  const setReference = (payload) => setState({ [timestamp.id]: payload })

  // Checking if we initialized the timestamp state
  if (!reference()) {
    // Creating a view manager for the timestamp
    const views = timestamps.createViewManager()

    // Creating a form manager for the timestamp
    const forms = timestamps.createFormManager(timestamp)

    // Creating a list of onEvent handlers for the timestamp
    const events = timestamps.createOnEvents(timestamp, views, forms, actions)

    // Finalizing timestamp state
    setReference({ events, forms, views })
  }

  // Returning the timestamp state
  return reference()
}
