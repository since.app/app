// Package imports
import { createState } from 'solid-js'

// Application imports
import { merge } from '--/utils'
import { use as i18n } from '--/store/i18n'
import UpdateForm from '--/forms/timestamp/update'

/**
 * Closed timestamp view.
 *
 * @type {String}
 */
export const VIEW_CLOSED = 'closed'

/**
 * Timestamp details view.
 *
 * @type {String}
 */
export const VIEW_DETAILS = 'details'

/**
 * Timestamp local sync status details view.
 *
 * @type {String}
 */
export const VIEW_LOCAL = 'local'

/**
 * Timestamp remote sync status details view.
 *
 * @type {String}
 */
export const VIEW_REMOTE = 'remote'

/**
 * Timestamp update title/description form view.
 *
 * @type {String}
 */
export const VIEW_UPDATE = 'update'

/**
 * Creates a form manager for the given timestamp.
 *
 * Instead of creating every timestamp form and caching them all at once, this manager
 * returns a list of functions that do it for each individual form.
 *
 * @function            createFormManager
 * @param    {Object}   timestamp         Timestamp to create forms for
 * @param    {Object}   state             Volatile timestamp state data
 * @param    {Function} setState          Volatile timestamp state setter
 * @returns  {Object}                     Form invokers
 */
export function createFormManager (timestamp) {
  const [state, setState] = createState()

  // Helper function that caches and returns the cached form
  const cache = (form) => setState(form) || form

  return {
    update: () => state.update || cache({ update: UpdateForm({ timestamp }) }).update
  }
}

/**
 * Creates the list of click events for interacting with timestamps.
 *
 * @function          createOnEvents
 * @returns  {Object}                List of on* events
 */
export function createOnEvents (timestamp, views, forms, actions) {
  const { deleteTimestamp, duplicateTimestamp, updateTimestamp } = actions
  const [tr] = i18n()

  const updates = forms.update()

  return {
    /**
     * Attempts to close the edit menu.
     */
    onCancel () {
      if (!updates.dirty()) {
        // Go back to expanded view if there are no changes
        views.details()
      } else if (window.confirm(tr.timestamp.confirm.discard)) {
        // Clear any unsaved form data
        updates.reset()

        // Go back to expanded view
        views.details()
      }
    },

    /**
     * Closes the timestamp details.
     */
    onClose () {
      views.closed()
    },

    /**
     * Attempts to delete a timestamp.
     */
    onDelete () {
      if (window.confirm(tr.timestamp.confirm.delete)) {
        // Deleting the timestamp deletes the DOM node, i.e. closes the menu
        deleteTimestamp(timestamp)
      }
    },

    /**
     * Duplicates the current timestamp.
     */
    onDuplicate () {
      duplicateTimestamp(timestamp)

      // Close the current timestamp
      views.closed()
    },

    /**
     * Displays the local sync status view.
     */
    onLocal () {
      views.local()
    },

    /**
     * Displays the timestamp details view.
     */
    onOpen () {
      views.details()
    },

    /**
     * Displays the remote sync status view.
     */
    onRemote () {
      views.remote()
    },

    /**
     * Attempts to save basic changes made to a timestamp.
     */
    onSave () {
      if (!updates.dirty()) {
        // Go back to expanded view if there are no changes
        views.details()
      } else if (updates.validate()) {
        // Update our timestamp data
        updateTimestamp(merge(timestamp, updates.dirty(true)))

        // Make sure the saved values persist in the form
        updates.save()

        // Go back to expanded view
        views.details()
      }
    },

    /**
     * Displays the timestamp update form view.
     */
    onUpdate () {
      views.update()
    }
  }
}

/**
 * Creates a view state manager for our timestamp
 *
 * @returns {Object} View state getter and state setters
 */
export function createViewManager () {
  const [state, setState] = createState()

  // Generator function that sets the current view for the timestamp that's bound to setState
  const views = (view) => () => setState({ view })

  // Generator function to check if the current timestamp is showing a certain view
  const is = (view) => () => state.view === view

  // Default timestamp view is closed
  views(VIEW_CLOSED)()

  // Return the list of view switching and checking functions
  return {
    closed: views(VIEW_CLOSED),
    details: views(VIEW_DETAILS),
    local: views(VIEW_LOCAL),
    remote: views(VIEW_REMOTE),
    update: views(VIEW_UPDATE),
    is: {
      closed: is(VIEW_CLOSED),
      details: is(VIEW_DETAILS),
      local: is(VIEW_LOCAL),
      remote: is(VIEW_REMOTE),
      update: is(VIEW_UPDATE)
    }
  }
}
