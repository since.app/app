// Application imports
import { transpose, uuidv4 } from '--/utils'

export function inject (actions, setState) {
  Object.assign(actions, {
    createTag: (data) => createTag(data, setState),
    deleteTag: (data) => deleteTag(data, setState),
    duplicateTag: (data) => duplicateTag(data, setState),
    updateTag: (data) => updateTag(data, setState)
  })
}

export function createTag (data, setState) {
  console.log('store.data.tag.createTag');
  if (!data.id) data.id = uuidv4()

  setState(data.id, data)
}

export function deleteTag (data, setState) {
  console.log('store.data.tag.deleteTag');
  if (!data.id) throw new Error('Cannot delete tag: no "id" found')

  setState(data.id, undefined)
}

export function duplicateTag (data, setState) {
  console.log('store.data.tag.duplicateTag');
  // Transpose to remove reactive references
  const duplicate = transpose(data, { id: uuidv4() })

  setState(duplicate.id, duplicate)
}

export function updateTag (data, setState) {
  console.log('store.data.tag.updateTag');
  if (!data.id) throw new Error('Cannot update tag: no "id" found')

  setState(data.id, data)
}
