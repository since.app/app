// Application imports
import { make } from '--/store/drivers/local'

/**
 * Creates an offline persistence.
 *
 * Current implementation uses window.localStorage.
 *
 * @param   {String} name Browser Local Storage key to use
 * @returns {Array}       State getters and setters tuple
 */
export default function local (name) {
  // Create our localStorage instance
  const [{ stored, synced }, { pull, push, sync }] = make(name)

  return [
    { stored, synced },
    { pull, push, sync }
  ]
}
