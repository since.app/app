// Application imports
import lokka from '--/api/data'
import { make } from '--/store/drivers/lokka'
import * as timestamp from './lokka/timestamp'

/**
 * Creates a remote persistence.
 *
 * Current implementation uses Lokka GraphQL.
 *
 * @function         remote
 * @returns  {Array}        State getters and setters tuple
 */
export default function remote () {
  const [{ stored, synced }, { flush, pull, push, sync }] = make(lokka())

  // Helper function that returns the seperate models types(tag/timestamp)
  const filter = (array) => {
    return {
      tags: array.filter((item) => !item.date),
      timestamps: array.filter((item) => item.date)
    }
  }

  // Shared functions that persist changes via Lokka/GraphQL
  const persist = {
    created (db, deltas, setStored, setSynced) {
      const { timestamps } = filter(deltas)
      if (timestamps.length) timestamp.createTimestamps(db, timestamps, setStored, setSynced)
    },
    deleted (db, deltas, setStored, setSynced) {
      const { timestamps } = filter(deltas)
      if (timestamps.length) timestamp.deleteTimestamps(db, timestamps, setStored, setSynced)
    },
    updated (db, deltas, setStored, setSynced) {
      const { timestamps } = filter(deltas)
      if (timestamps.length) timestamp.updateTimestamps(db, timestamps, setStored, setSynced)
    }
  }

  return [
    { stored, synced },
    {
      flush,
      pull,
      push: (payload) => push(payload, persist),
      sync: (payload) => sync(payload, persist)
    }
  ]
}
