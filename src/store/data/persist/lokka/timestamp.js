import { clone, expunge, is, iterate, merge, minify, zip } from '--/utils'
import { createSetters, getGraphError } from '--/store/drivers/lokka'

// Helper function that removes the date created and updated properties
const prepare = (data) => expunge(data, { created: true, updated: true })

/**
 * Creates a timestamp on our GraphQL server.
 *
 * @function               createTimestamp
 * @param    {Object}      lokka           Lokka connection interface
 * @param    {Object}      data            Timestamp to create
 * @param    {Function}    setStored       Stored state setter
 * @param    {Function}    setSynced       Synced state setter
 * @returns  {Object|void}                 Created timestamp
 * @throws   {Error}                       Non-GraphQL error
 */
export async function createTimestamp (lokka, data, setStored, setSynced) {
  const { store, sync } = createSetters(data, setStored, setSynced)

  try {
    // Push timestamp to sync state to show we're trying to create it
    sync(false)

    // Persist the new timestamp on our server
    const query = await lokka.mutate(minify(`
      ($input: TimestampCreate!) {
        createTimestamp(input: $input) {
          updated
        }
      }
    `), { input: prepare(data) })

    // Server always controls the update time
    const results = query.createTimestamp

    // Registering with two accounts using the same timestamps would overwrite
    // the user association of the other. To prevent this, the server can send
    // us a new UUID
    const created = clone({ ...data, ...results })

    // Set timestamp as synced
    store(created)

    // Return the existing data with the updated timestamp
    return created
  } catch (e) {
    // Return undefined for failed queries but throw non-GraphQL errors
    sync(getGraphError(e))
  }
}

/**
 * Creates multiple timestamps on our GraphQL server.
 *
 * @function              createTimestamps
 * @param    {Object}     lokka            Lokka connection interface
 * @param    {Object}     data             Timestamps to create
 * @param    {Function}   setStored        Stored state setter
 * @param    {Function}   setSynced        Synced state setter
 * @returns  {Array|void}                  Created timestamps
 * @throws   {Error}                       Non-GraphQL error
 */
export async function createTimestamps (lokka, data, setStored, setSynced) {
  // Forward calls to create single timestamps
  if (!is.arr(data)) return [createTimestamp(...arguments)]
  if (is.len(data, 1)) return [createTimestamp(lokka, data[0], setStored, setSynced)]

  const { store, sync } = createSetters(data, setStored, setSynced)

  try {
    // Flag all created timestamps as sync in progress
    sync(false)

    // Persist multiple timestamps on our server
    const query = await lokka.mutate(minify(`
      ($input: [TimestampCreate!]!) {
        createTimestamps(input: $input) {
          updated
        }
      }
    `), { input: data.map(prepare) })

    // Timestamps successfully created on server
    const results = query.createTimestamps

    // Update the list of created timestamps with the server update time
    const created = data.map((item, i) => clone({ ...item, ...results[i] }))

    // Persist changes in state
    store(created)

    // Return updated timestamps
    return created
  } catch (e) {
    // Set timestamp error status and return undefined
    sync(getGraphError(e))
  }
}

/**
 * Deletes a timestamp on our GraphQL server.
 *
 * @function               deleteTimestamp
 * @param    {Object}      lokka           Lokka connection interface
 * @param    {Object}      data            Timestamp to delete
 * @param    {Function}    setStored       Stored state setter
 * @param    {Function}    setSynced       Synced state setter
 * @returns  {Object|void}                 Deleted timestamp
 * @throws   {Error}                       Non-GraphQL error
 */
export async function deleteTimestamp (lokka, data, setStored, setSynced) {
  const { store, sync } = createSetters(data, setStored, setSynced)

  try {
    // Set timestamp as being synchronized
    sync(false)

    // Attempt to delete the timestamp on the server
    await lokka.mutate(minify(`
      ($input: ID!) {
        deleteTimestamp(input: $input) {
          updated
        }
      }
    `), { input: data.id })

    // Remove timestamp from states
    store(undefined)

    // For consistency between server and client, we return the successfully deleted object
    return data
  } catch (e) {
    // Set timestamp error status and return undefined
    sync(getGraphError(e))
  }
}

/**
 * Deletes multiple timestamps on our GraphQL server.
 *
 * @function              deleteTimestamps
 * @param    {Object}     lokka            Lokka connection interface
 * @param    {Object}     data             Timestamps to delete
 * @param    {Function}   setStored        Stored state setter
 * @param    {Function}   setSynced        Synced state setter
 * @returns  {Array|void}                  Deleted timestamps
 * @throws   {Error}                       Non-GraphQL error
 */
export async function deleteTimestamps (lokka, data, setStored, setSynced) {
  if (!is.arr(data)) return [deleteTimestamp(...arguments)]
  if (is.len(data, 1)) return [deleteTimestamp(lokka, data[0], setStored, setSynced)]

  const { store, sync } = createSetters(data, setStored, setSynced)

  try {
    // Set timestamp as being synchronized
    sync(false)

    // Attempt to delete the timestamp on the server
    await lokka.mutate(minify(`
      ($input: [ID!]!) {
        deleteTimestamps(input: $input) {
          updated
        }
      }
    `), { input: data.map((item) => item.id) })

    // Remove timestamp from states
    store(undefined)

    // For consistency between server and client, we return the successfully deleted object
    return data
  } catch (e) {
    // Set timestamp error status and return undefined
    sync(getGraphError(e))
  }
}

/**
 * Fetches all timestamps on our GraphQL server.
 *
 * @function              queryTimestamps
 * @param    {Object}     lokka           Lokka connection interface
 * @returns  {Array|void}                 Remote persisted timestamps
 * @throws   {Error}                      Non-GraphQL error
 */
export async function queryTimestamps (lokka) {
  try {
    const results = await lokka.query(minify(`
      {
        timestamps {
          id title description date created updated
        }
      }
    `))

    return results.timestamps
  } catch (e) {
    // Return undefined for failed queries but throw non-GraphQL errors
    console.log(getGraphError(e))
  }
}

/**
 * Updates a timestamp on our GraphQL server.
 *
 * @function               updateTimestamp
 * @param    {Object}      lokka           Lokka connection interface
 * @param    {Object}      data            Timestamp to update
 * @param    {Function}    setStored       Stored state setter
 * @param    {Function}    setSynced       Synced state setter
 * @returns  {Object|void}                 Updated timestamp
 * @throws   {Error}                       Non-GraphQL error
 */
export async function updateTimestamp (lokka, data, setStored, setSynced) {
  const { store, sync } = createSetters(data, setStored, setSynced)

  try {
    // Flag timestamp as being synchronized
    sync(false)

    // Persist changes to server
    const query = await lokka.mutate(minify(`
      ($input: TimestampUpdate!) {
        updateTimestamp(input: $input) {
          updated
        }
      }
    `), { input: prepare(data) })

    // Server always controls the update time
    const results = query.updateTimestamp

    // Update model data
    const updated = { ...clone(data), ...results }

    // Update stored state and sync status
    store(updated)

    // Return the updated timestamp
    return updated
  } catch (e) {
    // Set timestamp error status and return undefined
    sync(getGraphError(e))
  }
}

/**
 * Updates multiple timestamps on our GraphQL server.
 *
 * @function              updateTimestamps
 * @param    {Object}     lokka            Lokka connection interface
 * @param    {Object}     data             Timestamps to update
 * @param    {Function}   setStored        Stored state setter
 * @param    {Function}   setSynced        Synced state setter
 * @returns  {Array|void}                  Updated timestamps
 * @throws   {Error}                       Non-GraphQL error
 */
export async function updateTimestamps (lokka, data, setStored, setSynced) {
  // Forward calls to update single timestamps
  if (!is.arr(data)) return [updateTimestamp(...arguments)]
  if (is.len(data, 1)) return [updateTimestamp(lokka, data[0], setStored, setSynced)]

  const { store, sync } = createSetters(data, setStored, setSynced)

  try {
    // Flagging timestamps as being updated
    sync(false)

    // Persist changes to server
    const query = await lokka.mutate(minify(`
      ($input: [TimestampUpdate!]!) {
        updateTimestamps(input: $input) {
          updated
        }
      }
    `), { input: data.map(prepare) })

    const results = query.updateTimestamps

    const updated = data.map((item, i) => clone({ ...item, ...results[i] }))

    store(updated)

    return updated
  } catch (e) {
    sync(getGraphError(e))
  }
}
