// Package imports
import { createState } from 'solid-js'

// Application imports
import createLocal from './local'
import createRemote from './remote'

/**
 * Creates a persistent store.
 *
 * @function          persist
 * @param    {String} name    Browser Local Storage key to use
 * @returns  {Array}          Persistence state and actions tuple
 */
export default function persist (name) {
  // Create our localStorage persistence
  const [{ stored: localStored, synced: localSynced }, local] = createLocal(name)

  // Create our database persistence
  const [{ stored: remoteStored, synced: remoteSynced }, remote] = createRemote()

  // Create the list of persistence sync statuses
  const [synced] = createState({ local: localSynced, remote: remoteSynced })

  // Allow access to stored data
  const stored = { local: localStored, remote: remoteStored }

  return [
    { stored, synced },
    { local, remote }
  ]
}
