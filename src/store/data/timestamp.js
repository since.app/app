// Application imports
import { merge, uuidv4 } from '--/utils'

/**
 * Duplicates a timestamp in our state.
 *
 * @function            cloneTimestamp
 * @param    {Object}   data           Timestamp to duplicate
 * @param    {Function} setState       State setter function
 */
export function cloneTimestamp (data, setState) {
  // A full timestamp clone is just a UUID replacement
  const model = merge(data, { id: uuidv4() })

  setState(model.id, model)
}

/**
 * Creates default timestamp values.
 *
 * @function          createDefaults
 * @returns  {Object}                Default values
 */
export function createDefaults () {
  return { id: uuidv4(), date: Date.now(), created: Date.now() }
}

/**
 * Creates a timestamp and adds it to our state.
 *
 * @function            createTimestamp
 * @param    {Object}   data            Created timestamp data
 * @param    {Function} setState        State setter
 */
export function createTimestamp (data, setState) {
  // Merge data into defaults to preserve client UUIDs
  const model = merge(createDefaults(), data)

  setState(model.id, model)
}

/**
 * Deletes a timestamp from our state.
 *
 * @function            deleteTimestamp
 * @param    {Object}   data            Timestamp to delete
 * @param    {Function} setState        State setter function
 * @throws   {Error}                    Missing UUID
 */
export function deleteTimestamp (data, setState) {
  if (!data.id) throw new Error('Cannot delete timestamp: no "id" found')

  setState(data.id, undefined)
}

/**
 * Duplicates a timestamp with a new date attributes in our state.
 *
 * @function            duplicateTimestamp
 * @param    {Object}   data               Timestamp to duplicate
 * @param    {Function} setState           State setter function
 */
export function duplicateTimestamp (data, setState) {
  // Merge new defaults onto current timestamp to make a "just now" copy
  const model = merge(data, createDefaults())

  setState(model.id, model)
}

/**
 * Binds create/update/delete functions to a state by overloading the actions object.
 *
 * @function            inject
 * @param    {Object}   actions  Actions object to overload
 * @param    {Function} setState State setter function
 */
export function inject (actions, setState) {
  Object.assign(actions, {
    cloneTimestamp: (data) => cloneTimestamp(data, setState),
    createTimestamp: (data) => createTimestamp(data, setState),
    deleteTimestamp: (data) => deleteTimestamp(data, setState),
    duplicateTimestamp: (data) => duplicateTimestamp(data, setState),
    updateTimestamp: (data) => updateTimestamp(data, setState)
  })
}

/**
 * Updates a timestamp in our state.
 *
 * @function            updateTimestamp
 * @param    {Object}   data            Updated timestamp
 * @param    {Function} setState        State setter function
 */
export function updateTimestamp (data, setState) {
  if (!data.id) throw new Error('Cannot update timestamp: no "id" found')

  setState(data.id, merge(data, { updated: Date.now() }))
}
