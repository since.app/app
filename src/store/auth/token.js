// Package imports
import decode from 'jwt-decode'

// Application imports
import { clone, minify } from '--/utils'
import { createSetters, getGraphError } from '../auth'

/**
 * Creates a JWT session.
 *
 * @param {Object}   data          Input payload
 * @param {String}   data.username Username input
 * @param {String}   data.password Password input
 * @param {Function} loader        Resource state loader
 * @param {Lokka}    lokka         GraphQL client
 */
export async function login (lokka, data, setStored, setSynced) {
  const { auth, store } = createSetters(setStored, setSynced)

  try {
    // Flag all authentication states as loading
    auth(false)

    // Create a JWT to access data
    const results = await lokka.mutate(minify(`
      ($input: TokenCreate!) {
        createToken(input: $input) {
          token bearer expires
        }
      }
    `), { input: data })

    // Get the created token
    const token = results.createToken

    // Store the received token(bearer and refresh) and decode the JWT for the user's nickname
    store({ token, user: decodeUser(token.bearer) })

    // Return the created token
    return token
  } catch (e) {
    // Make sure we only catch GraphQL errors and flag everything related to authentication as failed
    auth(getGraphError(e))
  }
}

/**
 * Returns a user details object from a bearer JWT.
 *
 * @param   {String} token Token to decode
 * @returns {Object}       User details
 */
export function decodeUser (token) {
  // Decode the JWT to get user info
  const jwt = decode(token)

  // Create a user object from said info
  return { id: jwt.sub, name: jwt.name, user: jwt.user }
}

/**
 * Destroys a JWT session.
 *
 * @param {Object}   data       Input payload
 * @param {String}   data.token Token input
 * @param {Function} loader     Resource state loader
 * @param {Lokka}    lokka      GraphQL client
 */
export async function logout (lokka, data, setStored, setSynced) {
  const { auth } = createSetters(setStored, setSynced)

  try {
    // Flag all authentication states as loading
    auth(false)

    // Immediately log the user out of the client side
    setStored({ token: {}, user: {} })

    // Attempt to lot the user out of the server side
    await lokka.mutate(minify(`
      ($input: TokenDelete!) {
        deleteToken(input: $input) {
          updated
        }
      }
    `), { input: data })

    // Update the authentication state to flag it as disconnected
    auth(undefined)

    // For consistency between server and client, we return the successfully deleted object
    return data
  } catch (e) {
    // Set the error on all authentication and return undefined
    auth(getGraphError(e))
  }
}

/**
 * Injects token authentication actions.
 *
 * @param {Object}   actions Actions object to populate
 * @param {Function} loader  Resource state load function
 * @param {Lokka}    lokka   GraphQL client
 */
export function inject (lokka, actions, setStored, setSynced) {
  Object.assign(actions, {
    email: (data) => email(lokka, data, setStored, setSynced),
    login: (data) => login(lokka, data, setStored, setSynced),
    logout: (data) => logout(lokka, data, setStored, setSynced),
    refresh: (data) => refresh(lokka, data, setStored, setSynced),
    validate: (data) => validate(lokka, data, setStored, setSynced)
  })
}

/**
 * Refresh a JWT session.
 *
 * @param {Object}   data       Input payload
 * @param {String}   data.token Token input
 * @param {Function} loader     Resource state loader
 * @param {Lokka}    lokka      GraphQL client
 */
export async function refresh (lokka, data, setStored, setSynced) {
  const { auth, store, sync } = createSetters(setStored, setSynced)

  try {
    // Flag token as not synchronized while we do exactly that
    sync({ token: false })

    const results = await lokka.mutate(minify(`
      ($input: TokenUpdate!) {
        updateToken(input: $input) {
          bearer expires
        }
      }
    `), { input: data })

    // Don't replace our refresh token
    const token = { ...clone(data), ...results.updateToken }

    // Update our state stored token
    store({ token })

    // Return the updated token
    return token
  } catch (e) {
    // Any failed attempt to refresh the token invalidates the current session
    store({ token: {}, user: {} })

    // Flag all authentication states as failed
    auth(getGraphError(e))
  }
}

export async function email (lokka, data, setStored, setSynced) {
  const { auth, store } = createSetters(setStored, setSynced)

  try {
    // Flag authentication as loading while we validate the token
    auth(false)

    // Delete the registration required token which returns a new JWT session
    const results = await lokka.mutate(minify(`
      ($input: TokenDelete!) {
        deleteToken(input: $input) {
          token bearer expires
        }
      }
    `), { input: data })

    // Replace our entire registration token data with the JWT session data
    const token = results.deleteToken

    // Set the validated token and user info
    store({ token, user: decodeUser(token.bearer) })

    // Return the new JWT session
    return token
  } catch (e) {
    auth(getGraphError(e))
  }
}

/**
 * Validates a JWT session.
 *
 * @param {Object}   data       Input payload
 * @param {String}   data.token Token input
 * @param {Function} loader     Resource state loader
 * @param {Lokka}    lokka      GraphQL client
 */
export async function validate (lokka, data, setStored, setSynced) {
  const { auth, store } = createSetters(setStored, setSynced)

  try {
    // Flag all authentication states as loading
    auth(false)

    // Attempt to validate a token by updating its expire time
    const results = await lokka.mutate(minify(`
      ($input: TokenUpdate!) {
        updateToken(input: $input) {
          bearer expires
        }
      }
    `), { input: data })

    // Don't replace our refresh token
    const token = { ...clone(data), ...results.updateToken }

    // Set the validated token and user info
    store({ token, user: decodeUser(token.bearer) })

    // Return the validate token's entire token object
    return token
  } catch (e) {
    // Make sure we only catch GraphQL errors and flag everything related to authentication
    auth(getGraphError(e))
  }
}
