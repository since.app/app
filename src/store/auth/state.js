
import { createState } from 'solid-js'

import { is } from '--/utils'
import { make as storage } from '--/store/drivers/local'
import LoginForm from '--/forms/auth/login'
import RegisterForm from '--/forms/auth/register'

export const VIEW_CLOSED = 'closed'

export const VIEW_LOGIN = 'login'

export const VIEW_LOGOUT = 'logout'

export const VIEW_REGISTER = 'register'

export const VIEW_VERIFY = 'verify'

export function createFormManager () {
  const [state, setState] = createState()
  const cache = (form) => setState(form) || form

  return {
    login: () => state.login || cache({ login: LoginForm() }).login,
    register: () => state.register || cache({ register: RegisterForm() }).register
  }
}

export function createViewManager () {
  const [state, setState] = createState()
  const views = (view) => () => setState({ view })
  const is = (view) => () => state.view === view

  views(VIEW_CLOSED)()

  return {
    closed: views(VIEW_CLOSED),
    login: views(VIEW_LOGIN),
    logout: views(VIEW_LOGOUT),
    register: views(VIEW_REGISTER),
    verify: views(VIEW_VERIFY),
    is: {
      closed: is(VIEW_CLOSED),
      login: is(VIEW_LOGIN),
      logout: is(VIEW_LOGOUT),
      register: is(VIEW_REGISTER),
      verify: is(VIEW_VERIFY)
    }
  }
}

export function inject (actions) {
  // Keep the user logged in by using localStorage
  const [{ stored: getLocal }, { pull, push: setLocal }] = storage('since.auth')

  // Attempt to load any data stored in the browser localStorage
  const ready = pull() === true

  // Volatile state data
  const [getState, setState] = createState()

  // Creating the form manager
  const forms = createFormManager()

  // Creating the view manager
  const views = createViewManager()

  // Setting authentication state
  setState({ forms, views })

  Object.assign(actions, {
    persist: (payload) => ready ? state(payload, getLocal, setLocal) : {},
    state: (payload) => state(payload, getState, setState)
  })
}

export function state (payload, getState, setState) {
  return is.obj(payload) ? (setState({ ...getState, ...payload }), getState) : getState
}
