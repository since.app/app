import { createSetters, getGraphError } from '../auth'
import { decodeUser } from './token'
import { minify } from '--/utils'

export function inject (lokka, actions, setStored, setSynced) {
  Object.assign(actions, {
    register: (data) => register(lokka, data, setStored, setSynced)
  })
}

export async function register (lokka, data, setStored, setSynced) {
  const { auth, store } = createSetters(setStored, setSynced)

  try {
    // Flag all authentication states as loading
    auth(false)

    // Registering returns an authenticated token
    const results = await lokka.mutate(minify(`
      ($input: UserCreate!) {
        createUser(input: $input){
          token bearer expires
        }
      }
    `), { input: data })

    // Get the created token
    const token = results.createUser

    // Store the received token
    store({ token, user: decodeUser(token.bearer) })

    // Successful authentication, no longer guest
    return token
  } catch (e) {
    // Make sure we only catch GraphQL errors and flag everything related to authentication as failed
    auth(getGraphError(e))
  }
}
