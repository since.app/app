import { ensure, uuidv4 } from '../utils'

export default class Tag {
  constructor ({ id, title, description, created, updated }) {
    this.id = id || uuidv4()
    this.title = title
    this.description = description
    this.created = ensure(created, Date)
    this.updated = ensure(updated, Date)
  }
}
