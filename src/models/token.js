export default class Token {
  constructor ({ token, bearer, expires }) {
    this.bearer = bearer
    this.expires = expires
    this.token = token
  }
}
