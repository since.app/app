export default class User {
  constructor ({ id, nickname, username }) {
    this.id = id
    this.nickname = nickname
    this.username = username
  }
}
