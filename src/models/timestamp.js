import { ensure, uuidv4 } from '../utils'

export default class Timestamp {
  constructor ({ id, title, description, date, created, updated }) {
    this.id = id || uuidv4()
    this.title = title
    this.description = description
    this.date = ensure(date, Date)
    this.created = ensure(created, Date)
    this.updated = ensure(updated, Date)
  }
}
