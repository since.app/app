// Package imports
import decode from 'jwt-decode'
import Http from 'lokka-transport-http'
import Lokka from 'lokka'

/**
 * Creates a Lokka instance with its default HTTP transport.
 *
 * @param   {String} url GraphQL endpoint
 * @returns {Lokka}     New Lokka instance
 */
export function http (url) {
  return make(new Http(url))
}

/**
 * Creates a Lokka instance with our custom JWT transport.
 *
 * @param   {String}   url     GraphQL endpoint
 * @param   {Function} refresh Function that returns a refresh JWT
 * @returns {Lokka}            New Lokka instance
 */
export function jwt (url, refresh) {
  return make(new TransportJwt(url, refresh))
}

/**
 * Creates a new Lokka instance with a given transport.
 *
 * @param   {LokkaHttpTransport} transport Lokka transport
 * @returns {Lokka}                        New Lokka instance
 */
export function make (transport) {
  return new Lokka({ transport })
}

/**
 * Custom JWT HTTP transport for Lokka.
 *
 * Can't use Lokka's own JWt transport due to a bug:
 * https://github.com/kadirahq/lokka-transport-jwt-auth/issues/3
 *
 * @class
 */
export class TransportJwt {
  /**
   * Lokka JWT HTTP transport constructor.
   *
   * @constructor
   * @param       {String}   url     GraphQL endpoint
   * @param       {Function} refresh Refresh function returning a JWT
   */
  constructor (url, refresh) {
    this.refresh = refresh
    this.url = url

    // Keeps track of our refresh token setTimeout ID
    this.refresher = null

    // Keeps track of refresh attempts to prevent flooding
    this.refreshes = []

    // Keeps an instance of our Lokka HTTP transport
    this.transport = null
  }

  /**
   * Sends a Lokka request via our JWT HTTP transport.
   *
   * @param   {mixed}           query     Lokka query to send
   * @param   {mixed}           variables Query variables to send
   * @param   {String}          opname    Query operation name
   * @returns {Promise<Object>}           Lokka response
   */
  async send (query, variables, opname) {
    return (await this.connect()).send(query, variables, opname)
  }

  /**
   * Attempts to connect our transport or refresh its JWT.
   *
   * @param   {Boolean}                     [refresh=false] Force refresh token
   * @returns {Promise<LokkaHttpTransport>}                 Lokka transport
   */
  async connect (refresh = false) {
    // Return connected transport unless forcing a refresh
    if (!refresh && this.transport) return this.transport

    // Prevent flooding token refreshes
    if (this.flooding()) throw new Error('Flooding detected')

    // Refresh returns undefined but we attempt to access the token as an object
    const token = await this.refresh() || {}

    try {
      // Decode expiration date and convert it to milliseconds
      const expires = decode(token.bearer).exp * 1000

      // Refresh the token sixty seconds before it expires
      this.refresher = setTimeout(() => this.connect(true), expires - Date.now() - 6e4)
    } catch (e) {
      // Ignore errors trying to schedule refreshes
    }

    // Always create the transport, even with an invalid bearer token
    this.transport = new Http(this.url, {
      headers: { Authorization: 'Bearer ' + token.bearer }
    })

    // Return the authorized transport
    return this.transport
  }

  /**
   * Clears any token refresh timeouts and destroys the Lokka HTTP transport.
   */
  disconnect () {
    this.refresher = clearTimeout(this.refresher)
    this.transport = null
  }

  /**
   * Detects flooding at 10 requests per second or greater.
   *
   * @returns {Boolean} Flooding detected
   */
  flooding () {
    const now = Date.now()

    // Allow 10 attempts at a query that might flood
    if (this.refreshes.push(now) > 10) {
      // Flooding detected when the last 10 requests fired within one second
      return now - this.refreshes.slice(-11)[0] < 1000
    }

    // Less than 10 requests made
    return false
  }
}
