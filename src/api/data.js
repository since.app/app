import { createSignal } from 'solid-js'
import { use as auth } from '--/store/auth'
import { jwt } from './lokka'

export default function data () {
  const [user, { refresh }] = auth()
  const [client, setClient] = createSignal()

  // Lokka instance used as connected flag
  let lokka

  // Disconnect function that clears our connection
  const disconnect = () => lokka && setClient(lokka = lokka._transport.disconnect())

  // Refresh function waits for a GraphQL update response before returnning the token
  const refreshes = () => async () => {
    // Attempt to update the token or destroy our Lokka instance
    return await refresh({ token: user.token.token }) || disconnect()
  }

  // Connect function that updates our ready signal
  const connect = () => lokka || setClient(lokka = jwt(process.env.SOLID_APP_DATA_URL, refreshes()))

  // Return the ready signal which resolves to Lokka or undefined
  return { client, connect, disconnect }
}
