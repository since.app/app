import { http } from './lokka'

export default function auth () {
  return http(process.env.SOLID_APP_AUTH_URL)
}
