/**
 * Checks if the return value of the given access is not nullish.
 *
 * @param   {Function} accessor Function returning the value to check
 * @returns {Boolean}           True if not undefined or null
 */
export default function isset (accessor) {
  try {
    return accessor() !== undefined && accessor() !== null
  } catch (e) {
    // Catch not defined and read undefined errors
    return false
  }
}
