/**
 * Checks to see if a value has a given length or any length at all.
 *
 * @function           len
 * @param    {mixed}   value  Value to check
 * @param    {Number}  length Optional length to match
 * @returns  {Boolean}        Value has any or exactly the given length
 */
export default function len (value, length = undefined) {
  // Convert number into strings to count the number of characters
  if (typeof value === 'number') value = String(value)

  // Ensure we can access a length property
  if (!Object.hasOwnProperty.call(value, 'length')) throw new Error('value has no length')

  // Returns true if the value has any length or the exact length when specified
  return typeof length === 'number' ? value.length === length : value.length > 0
}
