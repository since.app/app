/**
 * Checks each constructor to see if the value is an instance of any of them.
 *
 * @function           a
 * @param    {mixed}   value        Value to check
 * @param    {Array}   constructors List of constructors to check
 * @returns  {Boolean}              True if value is an instance of a constructor
 */
export default function a (value, ...constructors) {
  for (const constructor of constructors.flat()) {
    if (value instanceof constructor) return true
  }

  return false
}
