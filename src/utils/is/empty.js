/**
 * Checks if a given value is empty.
 *
 * @param   {mixed}   value Value to check
 * @returns {Boolean}       True if empty-ish
 */
export default function empty (value) {
  return typeof value === 'object' && value !== null
    ? (Array.isArray(value) ? value : Object.values(value)).length === 0
    : !value
}
