/**
 * Checks each constructor to see if the value is not an instance of any of them.
 *
 * @function           not
 * @param    {mixed}   value        Value to check
 * @param    {Array}   constructors List of constructors to check
 * @returns  {Boolean}              True if value is not an instance of any constructor
 */
export default function not (value, ...constructors) {
  for (const constructor of constructors.flat()) {
    if (value instanceof constructor) return false
  }

  return true
}
