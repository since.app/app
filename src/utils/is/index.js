// Application imports
import a from './a'
import empty from './empty'
import len from './len'
import not from './not'
import set from './isset'

// https://github.com/uuidjs/uuid/blob/master/src/regex.js
export const UUID = /^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i

export default {
  a,
  empty,
  len,
  not,
  arr (value) {
    return value && typeof value === 'object' && Array.isArray(value)
  },
  async (value) {
    return value instanceof Promise
  },
  bool (value) {
    return typeof value === 'boolean'
  },
  del (value) {
    Object.getOwnPropertySymbols(value).includes(Symbol.for('state-deleted'))
  },
  false (value) {
    return value === false
  },
  fn (value) {
    return typeof value === 'function'
  },
  key (value, key) {
    return Object.hasOwnProperty.call(value, key)
  },
  null (value) {
    return !set(typeof value === 'function' ? value : () => value)
  },
  num (value) {
    return /^\d+$/.test(value)
  },
  obj (value, strict = false) {
    return value && typeof value === 'object' && (!strict || !Array.isArray(value))
  },
  set (value) {
    return set(typeof value === 'function' ? value : () => value)
  },
  str (value) {
    return typeof value === 'string'
  },
  true (value) {
    return value === true
  },
  undef (value) {
    return value === undefined
  },
  uuid (value) {
    return typeof value === 'string' && UUID.test(value)
  }
}
