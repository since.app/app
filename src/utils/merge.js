// Application imports
import copy from './clone'
import is from './is'
import iterate from './iterate'

/**
 * Merges several sources onto the target, overriding any previous values.
 *
 * @function                 merge
 * @param    {Object}        target       Target object
 * @param    {Array|Object}  sources      Objects to merge into target
 * @param    {Boolean}      [clone=true] Clone object before merging
 * @returns  {Object}                    Merged target object
 */
export default function merge (target, sources, clone = true) {
  if (!is.obj(target, true)) throw new TypeError('merge requires "target" to be an object')
  if (!is.obj(sources)) throw new TypeError('merge requires "sources" to be an object or an array of objects')

  // Can't spread objects since it copies symbols
  const reference = clone ? copy(target) : target

  // Iterate through each source property to push
  iterate(is.arr(sources) ? sources : [sources], (source) => {
    iterate(source, (value, key) => Object.assign(reference, { [key]: value }))
  })

  return reference
}
