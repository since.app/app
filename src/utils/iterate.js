// Application imports
import is from './is'

/**
 * Helper function that calls a function on each array or object item.
 *
 * @function            iterate
 * @param    {Object}   over         Object or array to iterate over
 * @param    {Function} call         Function to call on each item
 * @param    {Boolean}  [keys=false] Change callback signature to (key, value) => void
 */
export default function iterate (source, callback, keys = false) {
  // Only work with iterable arguments
  if (!is.obj(source)) source = [source]

  // Run exec on each array/object value
  Object.keys(source).forEach((key) => {
    // Flip callback function argument signature if requesting keys first
    callback(keys ? key : source[key], keys ? source[key] : key)
  })
}
