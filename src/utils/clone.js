// Application imports
import is from './is'
import iterate from './iterate'

/**
 * Clones an object without copying symbols.
 *
 * The spread expression `{ ...source }` copies unwanted reactive symbols.
 *
 * @param   {Object} source Object to clone
 * @returns {Object}        Cloned object
 */
export default function clone (source) {
  // Don't need to clone non-objects/arrays
  if (!is.obj(source)) return source

  // Create a basic object to return
  const cloned = is.arr(source) ? [] : {}

  // Function that copies object values to our clone
  const copy = (key) => {
    const value = source[key]

    // Check if we need to recurse another object
    if (is.obj(value)) {
      cloned[key] = clone(value)
    } else {
      cloned[key] = value
    }
  }

  // Copy properties by iterating object keys
  iterate(source, copy, true)

  // Return a symbol free object
  return cloned
}
