/**
 * Returns an object with milliseconds elapsed converted to seconds, minutes, etc.
 *
 * This returns PHP date() inspired formats.
 *
 * @param   {Number} ms Milliseconds or date to convert
 * @returns {Object}
 */
export default function elapsed (ms) {
  // Shortcut function
  const floor = (n) => Math.floor(n)

  // Divisible
  const s = 1000
  const i = s * 60
  const h = i * 60
  const d = h * 24
  const w = d * 7
  const m = d * 30
  const y = m * 12

  // Convert milliseconds to other formats
  const milliseconds = ms
  const seconds = floor(ms / s)
  const minutes = floor(ms / i)
  const hours = floor(ms / h)
  const days = floor(ms / d)
  const weeks = floor(ms / w)
  const months = floor(ms / m)
  const years = floor(ms / y)

  return {
    milliseconds,
    v: milliseconds,
    seconds,
    s: seconds,
    minutes,
    i: minutes,
    hours,
    h: hours,
    days,
    d: days,
    weeks,
    w: weeks,
    months,
    m: months,
    years,
    y: years
  }
}
