import is from './is'
import iterate from './iterate'

/**
 * Returns the differences between two objects.
 *
 * @param   {Object} source Source object to compare
 * @param   {Object} target Target object to compare
 * @returns {Object}        Differences found in both objects
 */
export default function delta (source, target) {
  const deltas = {}

  // Compare function that pushed differences into our diffs object
  const compare = (key) => {
    const sSet = is.key(source, key)
    const sVal = sSet ? source[key] : undefined
    const tSet = is.key(target, key)
    const tVal = tSet ? target[key] : undefined

    // Make sure we're not converting undefined to null and vice-versa
    if (is.null(sVal) && is.null(tVal)) return

    if (sVal !== tVal) {
      if (is.obj(sVal) && is.obj(tVal)) {
        // Recurse any object values for differences
        const recurse = delta(sVal, tVal)

        if (!is.empty(recurse)) deltas[key] = recurse
      } else {
        deltas[key] = tVal
      }
    }
  }

  // Check target object contains all source keys and values
  if (is.obj(source)) iterate(source, compare, true)

  // Check source object contains all target keys and values
  if (is.obj(target)) iterate(target, compare, true)

  // Return the list of differences by key
  return deltas
}
