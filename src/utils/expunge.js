// Application imports
import copy from './clone'
import is from './is'
import iterate from './iterate'

/**
 * Deletes object keys from an object.
 *
 * @function                   expunge
 * @param   {Object}           target      Object to delete keys from
 * @param   {Object|Function}  sources     Properties to delete or function iterating keys
 * @param   {Boolean}         [clone=true] Clone target before deleting
 * @returns {Object}                       Updated target
 */
export default function expunge (target, sources, clone = true) {
  if (!is.obj(target, true)) throw new TypeError('transpose requires "target" to be an object')

  // DRY type error throwing function
  const invalid = (key, type) => {
    throw new TypeError(`Cannot expunge property "${key}": invalid type ${type}`)
  }

  // Default to returning an updated copy of the source object instead of modifying it directly
  const reference = clone ? copy(target) : target

  // Function handling deleting properties
  const deleteProperty = (value, key) => {
    // Allow different ways of expunging data based on the key's value
    switch (typeof value) {
      case 'boolean':
        // Easiest is to specify true as its value({ [key]: true })
        if (value) delete reference[key]

        break
      case 'function':
        // It's also possible to use a function({ [key]: (value, key) => true })
        deleteProperty(value(reference[key], key), key)

        break
      case 'object':
        // Use native array functions if manipulating arrays
        if (is.arr(value)) invalid(key, 'array')

        // Recurse objects to expunge deep properties
        throw new Error('Expunging deep properties not implemented')
      default:
        // Throw invalid type error
        invalid(key, typeof value)
    }
  }

  // Properties can be an object or a function
  switch (typeof sources) {
    case 'boolean':
      // Pass true as sources will delete all object properties
      if (sources) iterate(reference, deleteProperty)

      break
    case 'function':
      // Specifying a function will iterate over each source key and delete those returning true
      iterate(reference, (value, key) => deleteProperty(sources(value, key), key))

      break
    case 'object':
      // Passing an object will delete keys whose values are true
      iterate(sources, deleteProperty)

      break
    default:
      throw new TypeError('expunge "properties" must be an object or a function')
  }

  // Return the updated source or an updated clone of the source
  return reference
}
