// Application imports
import is from './is'
import pad from './pad'

/**
 * Returns a PHP `date()` like formatted string.
 *
 * @function           phpdate
 * @param   {Date}     date    Date to evaluate
 * @param   {String}   format  Display format
 * @param   {Object}   labels  Functions to create labels
 * @param   {Function} day     Gets the textual day name(e.g. Monday)
 * @param   {Function} month   Gets the textual month name(e.g. January)
 * @param   {Function} suffix  Gets the textual suffix to numbers(e.g. 1st)
 * @returns {String}           Formatted date string
 */
export default function phpdate (date, format, { day, month, suffix }) {
  const pm = date.getHours() > 12

  // PHP like date formats
  const chars = {
    l: day(date),
    F: month(date),
    j: date.getDate(),
    S: suffix(date),
    h: pad(pm ? date.getHours() - 12 : date.getHours()),
    i: pad(date.getMinutes()),
    s: pad(date.getSeconds()),
    a: pm ? 'pm' : 'am'
  }

  // Build final clock string
  return format.split('').reduce((s, c) => s + (is.set(() => chars[c]) ? chars[c] : c), '')
}
