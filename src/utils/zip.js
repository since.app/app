import is from './is'

// Allow zipping keys with shared values
export default function zip (keys, values) {
  if (!is.arr(keys)) throw new TypeError('zip "keys" must be an array')
  if (!is.arr(values)) values = Array(keys.length).fill(values)
  if (keys.length !== values.length) throw new TypeError('zip arguments must be of equal length')

  const zipped = {}

  keys.forEach((key, i) => { zipped[key] = values[i] })

  return zipped
}
