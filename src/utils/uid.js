/**
 * Generates a unique ID.
 *
 * @function          uid
 * @param    {Number} length UID length(up to 13)
 * @returns  {String}
 */
export default function uid (length = 7) {
  return Math.random().toString(16).slice(2, length + 2)
}
