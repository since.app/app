// Application imports
import is from './is'
import iterate from './iterate'

/**
 * Capitalizes each word of a string.
 *
 * Strings can also be converted from various cases(e.g. snake_cased). The following
 * types can be specified:
 *
 * <ul>
 *  <li>'camel' will convert "camelCasedStrings" into "Camel Cased Strings"</li>
 *  <li>'pascal' will convert "PascalCasedStrings" into "Pascal Cased Strings"</li>
 *  <li>'kebab' will convert "kebab-cased-strings" into "Kebab Cased Strings"</li>
 *  <li>'snake' will convert "snake_cased_strings" into "Snake Cased Strings"</li>
 *  </ul>
 *
 * Passing an array of cases will run each conversion before capitalizing words.
 *
 * @param   {String}       str      String to capitalize
 * @param   {String|Array} [cased=] Optional case type
 * @returns {String}                Capitalized string
 */
export default function capitalize (str, cased) {
  // Uppercases the first character of a word
  const upper = (word) => word[0].toUpperCase() + word.slice(1)

  // Convert a cased format into a proper title
  const process = (type) => {
    switch (type) {
      case 'camel':
      case 'pascal':
        str = str.replace(/([A-Z])/g, (char) => ' ' + char)
        break
      case 'kebab':
        str = str.replace('-', ' ')
        break
      case 'snake':
        str = str.replace('_', ' ')
        break
    }
  }

  // Allow converting multiple case types at once by passing an array
  if (is.str(cased)) cased = [cased]
  if (is.arr(cased)) iterate(cased, process)

  // Use map to upper case individual words
  return str.split(' ').map(upper).join(' ')
}
