/**
 * Removes all spaces from a GraphQL query.
 *
 * @function         minify
 * @param   {String} str    Query to minify
 * @returns {String}        Minified query
 */
export default function minify (str) {
  // Return a copy of the string
  return str.slice()
    // Reduce all whitespaces to one
    .replace(/[\s\t\n]+/g, ' ')
    // Collapse single characters like brackets
    .replace(/\s(\S)\s/g, '$1')
    // Split words with commas
    .replace(/(\w)\s(\w)/g, '$1,$2')
    // Remove remaining spaces
    .replace(/\s/g, '')
}
