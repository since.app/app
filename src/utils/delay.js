/**
 * Returns a promise that will resolve in a certain amount of time.
 *
 * @param  {Number} ms Number of milliseconds to wait
 */
export default function delay (ms) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}
