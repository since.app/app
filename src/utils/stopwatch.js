// Application imports
import elapsed from './elapsed'

/**
 * Converts milliseconds to stopwatch displays.
 *
 * See the following examples:
 *
 * 1. Initialization with 0 milliseconds passed:
 *
 * ```js
 *  const stopwatch = {
 *    y: 0, years: 0,
 *    m: 0, months: 0,
 *    d: 0, days: 0,
 *    h: 0, hours: 0,
 *    i: 0, minutes: 0,
 *    s: 0, seconds: 0
 *  }
 * ```
 *
 * 2. Arbitrary time since yesterday:
 *
 * ```js
 *  const stopwatch = {
 *    y: 0,
 *    m: 0,
 *    d: 0,
 *    h: 11,
 *    i: 23,
 *    s: 55
 *  }
 * ```
 *
 * 3. Since someone's birthday:
 *
 * ```js
 *  const stopwatch = {
 *    y: 29,
 *    m: 5,
 *    d: 23,
 *    h: 17,
 *    i: 55,
 *    s: 9
 *  }
 * ```
 *
 * @function          stopwatch
 * @param    {Number} ms        Time elapsed in milliseconds
 * @returns  {Object}           Time elapsed as stopwatch
 */
export default function stopwatch (ms) {
  const t = elapsed(ms > 0 ? ms : 0)

  // Remaining is what renders the stopwatch effect
  const milliseconds = t.v % 1000
  const seconds = t.s % 60
  const minutes = t.i % 60
  const hours = t.h % 24
  const days = t.d % 30
  const months = t.m % 12
  const years = t.y

  return {
    milliseconds,
    v: milliseconds,
    seconds,
    s: seconds,
    minutes,
    i: minutes,
    hours,
    h: hours,
    days,
    d: days,
    months,
    m: months,
    years,
    y: years
  }
}
