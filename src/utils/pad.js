/**
 * Pads a number with a leading zero.
 *
 * @param   {Number} n Number to pad
 * @returns {String}   Padded number
 */
export default function pad (n) {
  return n < 10 ? '0' + n : '' + n
}
