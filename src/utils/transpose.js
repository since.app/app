// Application imports
import copy from './clone'
import is from './is'
import iterate from './iterate'

/**
 * Copies the values of one object onto another without adding new properties.
 *
 * @param   {Object}                 target      Target object
 * @param   {Object|Array|Function}  sources     Transpose sources
 * @param   {Boolean}               [clone=true] Clone target before transposing
 * @returns {Object}                             Target object with updated values
 */
export default function transpose (target, sources, clone = true) {
  if (!is.obj(target, true)) throw new TypeError('transpose requires "target" to be an object')

  // Can't spread objects since it copies symbols
  const reference = clone ? copy(target) : target

  // Transpose only overrides values; use merge if pushing new keys
  const updateProperty = (value, key) => {
    if (is.key(reference, key)) Object.assign(reference, { [key]: value })
  }

  switch (typeof sources) {
    case 'function':
      // Passing a function iterates the target object and replaces values with the callback results
      iterate(reference, (value, key) => updateProperty(sources(value, key), key))

      break
    case 'object':
      // Iterate through each source property to override
      iterate(is.arr(sources) ? sources : [sources], (source) => iterate(source, updateProperty))

      break
    default:
      throw new TypeError('transpose requires "sources" to be an object, an array of objects, or a function')
  }

  return reference
}
