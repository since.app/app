// Application imports
import Validated from '../validated'
import { transpose } from '--/utils'
import * as transforms from '--/transformations/timestamp'
import * as validates from '--/validations/timestamp'

/**
 * Update basic timestamp details component.
 *
 * @function                Timestamp
 * @param    {Object|Model} timestamp Timestamp to display
 */
export default function Update (props) {
  // Create timestamp property validators
  const validate = transpose(validates.rules, validates)

  // Create timestamp property transformers
  const transform = transpose(transforms.rules, transforms)

  // Create our validated form
  const Form = Validated(validate, transform, props.timestamp)

  // Form render function that accepts additional <form> properties(e.g. onSubmit())
  const render = (props = {}) => (
    <Form.Form class='pr-3' {...props}>
      <Form.Text
        class='text-xl block border rounded w-full'
        label={false}
        name='title'
      />

      <Form.Area
        class='block w-full border rounded mt-3'
        label={false}
        name='description'
      />
    </Form.Form>
  )

  // Forward all form functions and include the render form function
  return { ...Form, render }
}
