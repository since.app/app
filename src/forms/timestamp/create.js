// Application imports
import Validated from '../validated'
import { length } from '--/validations/rules'
import { title } from '--/transformations/timestamp'
import { use as data } from '--/store/data'
import { use as i18n } from '--/store/i18n'

/**
 * Create timestamp component.
 */
export default function Create () {
  const [tr] = i18n()
  const [, { createTimestamp }] = data()

  // Only accept title with the quick create
  const Form = Validated({ title: length({ max: 80 }) }, { title })

  /**
   * Form submit event handler.
   *
   * @param {Event} event Event fired
   */
  const onSubmit = (event) => {
    // Prevent form from doing anything
    event.preventDefault()

    // The only thing we get is a title for this component
    const title = Form.values('title')

    // Make sure it's valid before creating a timestamp
    if (!Form.validate() || !title) return

    // Create the actual timestamp
    createTimestamp({ title })

    // Clear the form input
    Form.clear()
  }

  return (
    <Form.Form onSubmit={onSubmit}>
      <div class='flex'>
        <Form.Text name='title' label={false} error={false} class='flex-1 my-0 mr-3' />
        <Form.Submit label={tr.timestamp.create} class='ui.btn ui.btn-text ui.btn-blue' />
      </div>

      <div class='my-2 mx-8'>
        <Form.Error name='title' class='block text-red-600' />
      </div>
    </Form.Form>
  )
}
