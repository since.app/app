// Application imports
import Validated from '../validated'
import { transpose } from '--/utils'
import { use as auth } from '--/store/auth'
import { use as i18n } from '--/store/i18n'
import * as transforms from '--/transformations/auth'
import * as validates from '--/validations/auth'

/**
 * Login component.
 *
 * @function      Login
 * @returns {JSX}       Validated form
 */
export default function Login () {
  const [tr] = i18n()
  const [user] = auth()

  // Create login input validators
  const validate = transpose({ username: true, password: true }, validates)

  // Create login input transformers
  const transform = transpose({ username: true, password: true }, transforms)

  // Create login form
  const Form = Validated(validate, transform)

  // Reactive shortcuts to input labels
  const t = () => ({
    user: tr.auth.login.username,
    pass: tr.auth.login.password
  })

  // Return a form render function instead of rendering the form
  const render = (props = {}) => (
    <Form.Form class='ui.form pr-3 py-3' {...props}>
      <Form.Text
        name='username'
        label={false}
        error={t().user}
        placeholder={t().user}
      />

      <Form.Password
        class='mt-3'
        name='password'
        label={false}
        error={t().pass}
        placeholder={t().pass}
      />

      <div class='flex mt-3'>
        <div class='flex-1'>
          {user.errors.token && (
            <span class='text-red-600'>{user.errors.token}</span>
          )}
        </div>

        <div class='flex-shrink'>
          <Form.Submit
            disabled={user.loading.token}
            class='ui.btn ui.btn-text ui.btn-blue inline-flex'
          >
            {user.loading.token && (
              <span class='ui.spinner mr-2 border-white' />
            )}{tr.auth.login.submit}
          </Form.Submit>
        </div>
      </div>
    </Form.Form>
  )

  // Forward all form functions and include the render form function
  return { ...Form, render }
}
