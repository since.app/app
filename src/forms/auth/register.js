// Application imports
import Validated from '../validated'
import { transpose } from '--/utils'
import { use as auth } from '--/store/auth'
import { use as i18n } from '--/store/i18n'
import * as transforms from '--/transformations/auth'
import * as validates from '--/validations/auth'

/**
 * Register form.
 *
 * @function      Login
 * @returns {JSX}       Validated form
 */
export default function Register () {
  // Loading internationalization context
  const [tr] = i18n()
  const [user] = auth()

  // Create timestamp property validators
  const validate = transpose(validates.rules, validates)

  // Create timestamp property transformers
  const transform = transpose(transforms.rules, transforms)

  // Create registration form
  const Form = Validated(validate, transform)

  // Reactive shortcuts to input labels
  const t = () => ({
    nick: tr.auth.register.nickname,
    user: tr.auth.register.username,
    pass: tr.auth.register.password
  })

  // Form render function that accepts additional <form> properties(e.g. onSubmit())
  const render = (props = {}) => (
    <Form.Form class='ui.form py-3 pl-3' {...props}>
      <Form.Text
        name='nickname'
        label={false}
        placeholder={t().nick}
        error={t().nick}
      />

      <Form.Email
        class='mt-3'
        name='username'
        label={false}
        placeholder={t().user}
        error={t().user}
      />

      <Form.Password
        class='mt-3'
        name='password'
        label={false}
        placeholder={t().pass}
        error={t().pass}
      />

      <div class='flex mt-3'>
        <div class='flex-1'>
          {user.errors.user && (
            <span class='text-red-600'>{user.errors.user}</span>
          )}
        </div>

        <div class='flex-shrink'>
          <Form.Submit
            disabled={user.loading.user}
            class='ui.btn ui.btn-text ui.btn-blue'
          >
            {user.loading.user && (
              <span class='ui.spinner mr-2 border-white' />
            )}{tr.auth.register.submit}
          </Form.Submit>
        </div>
      </div>
    </Form.Form>
  )

  return { ...Form, render }
}
