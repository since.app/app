// Package imports
import { createState } from 'solid-js'
import { Show } from 'solid-js/dom'

// Application imports
import { capitalize, clone, expunge, is, transpose, uid, zip } from '--/utils'

/**
 * Creates a series of form components bound to validators.
 *
 * @function          ValidatedForm
 * @param    {Object} validators    Properties and associated validator
 * @param    {Object} transformers  Property transformers ran prior to validation
 * @param    {Object} init          Initial values for the fields
 * @returns  {JSX}
 */
export default function ValidatedForm (validators, transformers = {}, init = {}) {
  // Generate unique ID per form
  const fid = 'f-' + uid()

  // Our form state consists of inputs and errors for each validator
  const inits = { inputs: {}, errors: {} }

  // Initial committed data
  const initial = {}

  // List of attributes in our form
  const attributes = Object.keys(validators)

  // Build our state initializer
  attributes.forEach((name) => {
    const value = is.set(() => init[name]) ? init[name] : ''

    // Need to split input and commit initialization so they don't reference each other
    inits.inputs[name] = value
    initial[name] = value
  })

  // Create our form state
  const [state, setState] = createState(inits)
  const [committed, commit] = createState(initial)

  // ---------------------------------------------------------------------------
  // Helper functions

  /**
   * Accepts and returns a given value.
   *
   * @param  {mixed} value Value to return
   * @return {mixed}       Value
   */
  const carry = (value) => value

  /**
   * Generates a unique ID for an input name.
   *
   * @function          id
   * @param    {String} name Property name
   * @return   {String}      Generated ID
   */
  const id = (name) => fid + '-' + name

  /**
   * Convert property names to labels.
   *
   * @function          labelize
   * @param    {String} name     Property name
   * @return   {String}          Capitalized label
   */
  const labelize = (name) => capitalize(name, ['camel', 'kebab', 'snake'])

  /**
   * Transformers accessor.
   *
   * @function          transforms
   * @param    {String} name       Property name
   * @returns  {mixed}             Transformed property value
   */
  const transforms = (name) => transformers[name] ? transformers[name] : carry

  /**
   * Transforms, updates, and validates a given property name.
   *
   * @function          upudate
   * @param    {String} name    Property name
   */
  const update = (name, empty = true) => {
    // Runs any tranformations on the input values
    const value = transforms(name)(state.inputs[name])

    // Update state with tranformed values
    setState('inputs', name, value)

    // Validate empty values unless skipped
    if (value || empty) setState('errors', name, validates(name)(value))
  }

  /**
   * Validator accessor.
   *
   * @function            validates
   * @param    {String}   name      Property name to access
   * @returns  {Function}           Validator function
   */
  const validates = (name) => validators[name]

  // ---------------------------------------------------------------------------
  // Form base components

  /**
   * Form field error component.
   *
   * @function           Error
   * @param    {Object}  props         Error properties
   * @param    {String}  props.label   Optional property label
   * @param    {String}  props.name    Property name
   * @param    {Boolean} props.visible Toggle error visiblity
   * @returns  {JSX}
   */
  function Error (props) {
    const error = () => state.errors[props.name]
    const label = () => typeof props.label !== 'string'
      ? labelize(props.name)
      : props.label

    return (
      <Show when={props.visible && error()}>
        <span class={'ui.error ' + (props.class || '')}>
          {label()} {error()}
        </span>
      </Show>
    )
  }

  /**
   * Form field input component.
   *
   * @function                    Field
   * @param    {Object}           props       Input properties
   * @param    {String|false}     props.error Optional property error
   * @param    {String|undefined} props.group Optional wrapping div
   * @param    {String|false}     props.label Optional property label
   * @param    {String}           props.name  Property name
   * @param    {String}           props.type  Input type
   * @returns  {JSX}
   */
  function Field (props) {
    let { error, group, label } = props
    if (group === undefined) group = false
    if (label === undefined) label = true
    if (error === undefined) error = true

    // Properties to remove from our props forwarding
    const used = {
      group: true,
      label: true,
      error: true,
      name: true,
      id: true,
      value: true,
      class: true,
      classList: true,
      onBlur: true,
      onChange: true,
      onKeyUp: true
    }

    // Creates a copy of our props and removes items that we specify on the input
    const input = expunge({ ...props }, used, false)

    // Helper function that allow calling multiple nullable handlers
    const calls = (...call) => (e) => call.forEach((fn) => is.fn(fn) && fn(e))

    // Handler that updates our state when inputs receive a key up event.
    const onKeyUp = (e) => setState('inputs', props.name, e.target.value)

    // Handler that validates inputs when they are blurred.
    const onChange = (e) => onKeyUp(e) || update(props.name, false)

    return (
      <Wrap group={group}>
        <Label name={props.name} visible={label !== false} label={label} />
        <Input
          id={id(props.name)}
          value={state.inputs[props.name]}
          placeholder={props.placeholder}
          class={'ui.input ' + (props.class || '')}
          classList={{ invalid: state.errors[props.name] }}
          onBlur={calls(onChange, props.onBlur)}
          onChange={calls(onChange, props.onChange)}
          onKeyUp={calls(onKeyUp, props.onKeyUp)}
          {...input}
        />
        <Error name={props.name} visible={error !== false} label={error} />
      </Wrap>
    )
  }

  /**
   * Returns a textarea or an input tag.
   *
   * @function         Input
   * @param   {Object} props Tag properties
   * @returns {JSX}
   */
  function Input (props) {
    if (props.type === 'textarea') {
      const resize = (ref) => {
        // Timeout to let the DOM populate itself before evaluating
        setTimeout(() => { ref.style.height = ref.scrollHeight + 'px' }, 0)
      }

      // Removing invalid textarea properties
      const text = expunge(props, { type: true, value: true })

      return (
        <textarea ref={resize} {...text}>{props.value}</textarea>
      )
    } else {
      return (
        <input {...props} />
      )
    }
  }

  /**
   * Form field label component.
   *
   * @function           Label
   * @param    {Object}  props         Label properties
   * @param    {String}  props.label   Optional property label
   * @param    {String}  props.name    Property name
   * @param    {Boolean} props.visible Toggle label visiblity
   * @returns  {JSX}
   */
  function Label (props) {
    const label = () => typeof props.label !== 'string'
      ? labelize(props.name)
      : props.label

    return (
      <Show when={props.visible}>
        <label
          for={id(props.name)}
          class={'ui.label ' + (props.class || '')}
          classList={{ invalid: state.errors[props.name] }}
        >
          {label()}
        </label>
      </Show>
    )
  }

  /**
   * Enables wrapping form inputs with a <div class>.
   *
   * @param   {String|Array} children Actual form inputs
   * @returns {JSX}
   */
  function Wrap (props) {
    if (props.group === false) {
      return (
        <>{props.children}</>
      )
    } else {
      return (
        <div class={props.group}>{props.children}</div>
      )
    }
  }

  // ---------------------------------------------------------------------------
  // Form inputs

  /**
   * Form textarea input.
   *
   * @function          Area
   * @param    {Object} props Input properties
   * @returns  {JSX}
   */
  function Area (props) {
    return (
      <Field type='textarea' {...props} />
    )
  }

  /**
   * Form email input.
   *
   * @function          Email
   * @param    {Object} props Input properties
   * @returns  {JSX}
   */
  function Email (props) {
    return (
      <Field type='email' {...props} />
    )
  }

  /**
   * Form password input.
   *
   * @function          Password
   * @param    {Object} props    Input properties
   * @returns  {JSX}
   */
  function Password (props) {
    return (
      <Field type='password' {...props} />
    )
  }

  /**
   * Form text input.
   *
   * @function          Text
   * @param    {Object} props Input properties
   * @returns  {JSX}
   */
  function Text (props) {
    return (
      <Field type='text' {...props} />
    )
  }

  // ---------------------------------------------------------------------------
  // Other form elements

  /**
   * Form tag.
   *
   * @function          Form
   * @param    {Object} props          Form properties
   * @param    {Array}  props.children Form child elements
   * @returns  {JSX}
   */
  function Form (props) {
    return (
      <form id={fid} {...props}>
        {props.children}
      </form>
    )
  }

  /**
   * Form submit button.
   *
   * @function          Submit
   * @param    {String} label  Button label
   * @param    {Object} props  Button properties
   * @returns  {JSX}
   */
  function Submit (props) {
    const button = expunge(props, { label: true, disabled: true })
    const disabled = () => props.disabled === true
    const label = () => is.set(() => props.label) ? props.label : 'Submit'

    return (
      <button type='submit' disabled={disabled()} {...button}>
        {props.children ? props.children : label()}
      </button>
    )
  }

  // ---------------------------------------------------------------------------
  // Public form methods

  /**
   * Clears the form inputs and errors.
   *
   * @param {String} name Input to clear
   */
  const clear = (...inputs) => {
    (inputs.length ? inputs : attributes).forEach((name) => {
      setState('inputs', name, '')
      setState('errors', name, undefined)
    })
  }

  /**
   * Returns the uncommitted form data.
   *
   * @function           dirty
   * @returns  {Boolean}       True if changes exist
   */
  const dirty = (delta = false) => {
    const diffs = {}

    for (const name of attributes) {
      const value = state.inputs[name]

      if (value !== committed[name]) {
        if (!delta) return true
        diffs[name] = value
      }
    }

    return delta ? diffs : false
  }

  /**
   * Returns the list of errors for the given inputs(or all if none).
   *
   * @function                    errors
   * @param    {String}           name   Property name to access
   * @returns  {String|null}        First error message or null
   */
  const errors = (...inputs) => {
    if (!inputs.length) return Object.values(state.errors).filter((error) => error)
    if (inputs.length === 1) return state.errors[inputs[0]]
    return transpose(zip(inputs), state.errors, false)
  }

  /**
   * Returns the current form values as a map from the given inputs.
   *
   * @function             map
   * @param    {...String} inputs Inputs to return
   * @returns  {Object}           The mapped form state
   */
  const map = (...inputs) => {
    if (!inputs.length) return clone(state.inputs)
    return transpose(zip(inputs), state.inputs, false)
  }

  /**
   * Resets the form.
   */
  const reset = (...inputs) => {
    (inputs.length ? inputs : attributes).forEach((name) => {
      setState('inputs', name, is.set(() => committed[name]) ? committed[name] : '')
      setState('errors', name, undefined)
    })
  }

  /**
   * Commits changes for reset.
   */
  const save = (...inputs) => {
    (inputs.length ? inputs : attributes).forEach((name) => {
      commit(name, state.inputs[name])
    })
  }

  /**
   * Validates the entire form.
   *
   * @function           validate
   * @return   {Boolean}          Form is valid
   */
  const validate = (...inputs) => {
    const names = inputs.length ? inputs : attributes

    // Run each validator
    names.forEach((name) => update(name))

    // Make sure none of the error fields have messages in them
    return !names.some((name) => state.errors[name])
  }

  /**
   * Values accessor.
   *
   * @param   {String} name Property name to access
   * @returns {String}      Current input value
   */
  const values = (...inputs) => {
    if (!inputs.length) return Object.values(state.inputs)
    if (inputs.length === 1) return state.inputs[inputs[0]]
    return inputs.map((name) => state.inputs[name])
  }

  return {
    clear,
    dirty,
    errors,
    map,
    reset,
    save,
    validate,
    values,
    // Bound components
    Area,
    Email,
    Error,
    Form,
    Label,
    Password,
    Submit,
    Text
  }
}
