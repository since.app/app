// Package imports
import { createEffect, createState } from 'solid-js'

// Application imports
import { use as sync } from '--/store/sync'
import { elapsed, is, pad, stopwatch } from '--/utils'

/**
 * Stopwatch style time elapsed.
 *
 * @function               Stopwatch
 * @param    {Object}      props     Component properties
 * @param    {Number|Date} since     Component properties
 * @returns  {JSX}
 */
export default function Stopwatch ({ since }) {
  const [{ seconds, minutes }] = sync()
  const [state, setState] = createState({ display: '' })

  // Get the number of milliseconds since epoch from the timestamp
  let ts = since
  if (is.num(ts)) ts = ts * (is.len(ts, 13) ? 1 : 1000)
  if (is.a(ts, Date)) ts = new Date(ts).getTime()

  // Use the slower signal for timestamps older than one hour
  const date = elapsed(Date.now() - ts).h > 0 ? minutes : seconds

  // Subscribe to our sync() signal
  createEffect(() => {
    // Offset by one second since sync() is delayed a bit
    const w = stopwatch(date() - ts + 1000)
    let display = ''

    // Add faces if present
    if (w.y) display += pad(w.y) + 'y'
    if (w.m) display += pad(w.m) + 'm'
    if (w.d) display += pad(w.d) + 'd'
    if (w.h) display += pad(w.h) + 'h'
    if (w.i) display += pad(w.i) + 'm'

    // Prefix blank minutes if there are none
    if (!display.length) display = '00m'

    // Always show seconds
    setState('display', (display + pad(w.s) + 's').slice(0, 6))
  })

  return (
    <>{state.display}</>
  )
}
