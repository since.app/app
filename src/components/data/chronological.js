// Package imports
import { createMemo } from 'solid-js'
import { For } from 'solid-js/dom'

// Application imports
import Timestamp from '../timestamp'

/**
 * Main timestamp listing component.
 *
 * @function       Chronological
 * @returns  {JSX}
 */
export default function Chronological (props) {
  // Sorting by date ascending
  const desc = (a, b) => b.date - a.date

  // Create reactive getters that return the list of models
  const timestamps = createMemo(() => Object.values(props.payload).sort(desc), [], true)

  return (
    <For each={timestamps()}>
      {(timestamp) => (
        <Timestamp timestamp={timestamp} />
      )}
    </For>
  )
}
