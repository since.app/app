// Package imports
import { Show } from 'solid-js/dom'

export default function Debug (props) {
  return (
    <Show when={props.visible}>
      <pre>{JSON.stringify(props.payload, null, 2)}</pre>
    </Show>
  )
}
