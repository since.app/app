// Application imports
import Login from './login'
import Register from './register'
import User from './user'
import Verify from './verify'
import { use as auth } from '--/store/auth'

/**
 * Authentication component.
 *
 * @function       Auth
 * @returns  {JSX}      Validated form
 */
export default function Auth () {
  // Load our authentication service
  const [, { state }] = auth()

  const { views: { is: view } } = state()

  // Visible state is handled through classes instead of <Show/> so everything
  // can share the same height meaning the container doesn't jump when switching
  // between otherwise different heights
  return (
    <div class='sm:container mx-auto mt-24 p-3 rounded border'>
      <div class='ui.share-height'>
        <Register visible={view.register()} />

        <Login visible={view.login()} />

        <User visible={view.logout()} />

        <Verify visible={view.verify()} />
      </div>
    </div>
  )
}
