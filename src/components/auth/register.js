// Package imports
import { createEffect } from 'solid-js'

// Application imports
import { use as auth } from '--/store/auth'
import { use as i18n } from '--/store/i18n'

/**
 * Register component.
 *
 * @function          Register
 * @param    {Object} props    Component properties
 * @returns  {JSX}             Validated form
 */
export default function Register (props) {
  // Loading internationalization context
  const [tr] = i18n()
  const [, { register, guest, state }] = auth()

  const { forms, views } = state()
  const form = forms.register()

  // Form submit function
  const onSubmit = async (event) => {
    // Prevent form from redirecting
    event.preventDefault()

    // Make sure a guest is trying to register with a valid form
    if (!guest() || !form.validate()) return

    // Handle registration attempt and show the logout page if successful
    if (await register(form.map('nickname', 'username', 'password'))) views.logout()
  }

  // Reactive class that toggles component visibility
  const visible = () => 'ui.share ' + (props.visible && 'ui.visible')

  // Effect on props.visible
  createEffect(() => {
    // Clear the form whenever the user switching to the login view
    if (!props.visible) form.clear()
  })

  return (
    <>
      <div class={visible() + ' text-center'}>
        <a onClick={() => views.login()} class='py-2 inline-block w-48 underline'>
          {tr.auth.register.login}
        </a>
      </div>

      <div class={visible()}>
        {form.render({ onSubmit })}
      </div>
    </>
  )
}
