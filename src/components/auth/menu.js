import { Show } from 'solid-js/dom'

import { use as auth } from '--/store/auth'

export default function Menu () {
  const [, { guest, persist, state }] = auth()

  const { views } = state()
  const { is: view } = views

  // Toggle debug views which is also saved as an offline configuration option
  const onDebug = () => persist({ debug: !persist().debug ? true : undefined })

  // Toggle the authentication splash
  const onToggle = () => view.closed()
    ? guest()
      ? views.login()
      : views.logout()
    : views.closed()

  return (
    <div class='flex flex-row-reverse'>
      <a
        class='ui.btn ui.btn-blank mt-2 mr-3 relative z-30'
        classList={{ 'ui.btn-grey': guest(), 'ui.btn-blue': !guest() }}
        onClick={onToggle}
      >
        <i class='material-icons'>{view.closed() ? 'account_circle' : 'close'}</i>
      </a>

      <Show when={!view.closed()}>
        <a
          class='ui.btn ui.btn-blank mt-2 mr-3 relative z-30'
          classList={{ 'ui.btn-grey': !persist().debug, 'ui.btn-blue': persist().debug }}
          onClick={onDebug}
        >
          <i class='material-icons'>bug_report</i>
        </a>
      </Show>
    </div>
  )
}
