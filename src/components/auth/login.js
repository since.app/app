// Package imports
import { createEffect } from 'solid-js'

// Application imports
// import LoginForm from '--/forms/auth/login'
import { use as auth } from '--/store/auth'
import { use as i18n } from '--/store/i18n'

/**
 * Login component.
 *
 * @function      Login
 * @returns {JSX}       Validated form
 */
export default function Login (props) {
  // Loading internationalization context
  const [tr] = i18n()
  const [, { login, state }] = auth()

  const { forms, views } = state()
  const form = forms.login()

  // Login form submit handler
  const onSubmit = async (event) => {
    // Prevent form from redirecting
    event.preventDefault()

    // Make sure form is valid before continuing
    if (!form.validate()) return

    // Handle authentication attempt and show logout page if successful
    if (await login(form.map('username', 'password'))) views.logout()
  }

  // Reactive class that toggles component visibility
  const visible = () => 'ui.share ' + (props.visible && 'ui.visible')

  // Effect on props.visible
  createEffect(() => {
    // Clear the form whenever the user switching to the register view
    if (!props.visible) form.clear()
  })

  return (
    <>
      <div class={visible()}>
        {form.render({ onSubmit })}
      </div>

      <div class={visible() + ' text-center'}>
        <a onClick={() => views.register()} class='py-2 inline-block w-48 underline'>
          {tr.auth.login.register}
        </a>
      </div>
    </>
  )
}
