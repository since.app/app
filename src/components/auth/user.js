// Application imports
import { use as auth } from '--/store/auth'
import { use as i18n } from '--/store/i18n'

/**
 * Logged in user authentication component.
 *
 * @function          User
 * @param    {Object} props Component properties
 * @returns  {JSX}
 */
export default function User (props) {
  // Load internationalization and authentication contexts
  const [tr] = i18n()
  const [user, { logout, state }] = auth()

  const { views } = state()

  // Logout function that also destroys remote tokens
  const onClick = () => {
    // Delete refresh token from server
    logout({ token: user.token.token })

    // Close the authentication overlay
    views.closed()
  }

  // Reactive class that toggles component visibility
  const visible = () => 'ui.share ' + (props.visible && 'ui.visible')

  return (
    <>
      <div class={visible()}>
        <h3 class='text-lg'>{tr.auth.user.welcome} <strong>{user.user.name}</strong>!</h3>
        <p class='m-3 mt-0 text-gray-600 text-italic'>({user.user.user})</p>
      </div>

      <div class={visible() + ' text-center'}>
        <a onClick={onClick} class='py-2 inline-block w-48 underline'>
          {tr.auth.user.logout}
        </a>
      </div>
    </>
  )
}
