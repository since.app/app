
import { Show } from 'solid-js/dom'

import { use as auth } from '--/store/auth'

export default function Disclaimer () {
  const [, { persist }] = auth()

  return (
    <Show when={persist().accept !== true}>
      <div class='mb-3 px-3 pb-3 rounded rounded-t-none bg-orange-500 text-white'>
        This is an alpha implementation of <strong>Since.app</strong>. It is a fully
        functioning offline web application but there are currently no security or
        stability guarantees.

        <button
          type='button'
          class='ml-2 italic underline'
          onClick={() => persist({ accept: true })}
        >
          I understand, please hide this message.
        </button>
      </div>
    </Show>
  )
}
