import { Show } from 'solid-js'

import { use as auth } from '--/store/auth'

export default function Verify (props) {
  const [user] = auth()

  return (
    <Show when={props.visible}>
      {user.loading.token ? (
        <p>
          <span class='ui.spinner border-blue-600 mr-3' />
          Attempting to register...
        </p>
      ) : user.errors.token ? (
        <p class='text-red-600'>Failed to register: {user.errors.token}</p>
      ) : (
        <p class='text-blue-600 text-bold'>Thank-you for registering!</p>
      )}
    </Show>
  )
}
