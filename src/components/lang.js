// Package imports
import { For } from 'solid-js/dom'

// Application imports
import { use as i18n } from '--/store/i18n'

/**
 * Language selector for internationalization.
 *
 * @function       Lang
 * @returns  {JSX}
 */
export default function Lang () {
  const [, { getLanguages, setLanguage }] = i18n()
  const onChange = (e) => setLanguage(e.target.value)

  return (
    <div>
      Select language:
      <select onChange={onChange}>
        <For each={getLanguages()}>
          {(language) => (
            <option value={language}>{language}</option>
          )}
        </For>
      </select>
    </div>
  )
}
