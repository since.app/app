// Application imports
import Menu from './auth/menu'
import { phpdate } from '--/utils'
import { use as i18n } from '--/store/i18n'
import { use as sync } from '--/store/sync'

/**
 * Clock component.
 *
 * @function       Clock
 * @returns  {JSX}       Rendered clock
 */
export default function Clock () {
  // Current language
  const [tr, translate] = i18n()
  const [{ seconds }] = sync()

  // Reactive date/time display that updates every second
  const display = () => phpdate(seconds(), tr.clock.format, translate)

  return (
    <div class='flex'>
      <div class='flex-1 text-3xl p-3'>
        {display()}
      </div>

      <div class='flex-shrink'>
        <Menu />
      </div>
    </div>
  )
}
