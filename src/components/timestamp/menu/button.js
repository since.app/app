/**
 * Material menu button component.
 *
 * @function          Button
 * @param    {Object} props  Component properties
 * @returns  {JSX}
 */
export default function Button (props) {
  // Extracting static properties
  const { onClick } = props

  return (
    <button type='button' onClick={onClick} class={'ml-3 ui.btn ' + (props.class || 'ui.btn-blue')}>
      <i class='material-icons'>{props.icon}</i>
    </button>
  )
}
