// Package imports
import { Match, Switch } from 'solid-js/dom'

// Application imports
import Button from './button'
import { use as data } from '--/store/data'

/**
 * Timestamp dynamic menu component.
 *
 * @function          Menu
 * @param    {Object} props Component properties
 * @returns  {JSX}
 */
export default function Menu (props) {
  const [, { state }] = data()

  const { events, views: { is: view } } = state(props.timestamp)
  const { onCancel, onClose, onDuplicate, onDelete, onOpen, onSave, onUpdate } = events

  return (
    <Switch>
      <Match when={view.closed()}>
        <Button icon='settings' onClick={onOpen} class='ui.btn-blank ui.btn-blue' />
      </Match>

      <Match when={view.details()}>
        <Button icon='edit' onClick={onUpdate} />
        <Button icon='filter_none' onClick={onDuplicate} />
        <Button icon='close' onClick={onClose} class='ui.btn-grey' />
      </Match>

      <Match when={view.local() || view.remote()}>
        <Button onClick={onOpen} icon='close' class='ui.btn-grey' />
      </Match>

      <Match when={view.update()}>
        <Button icon='delete' onClick={onDelete} class='ui.btn-red' />
        <Button icon='save' onClick={onSave} />
        <Button icon='close' onClick={onCancel} class='ui.btn-grey' />
      </Match>
    </Switch>
  )
}
