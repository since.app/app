// Application imports
import StatusWrapped from './persistence/status-wrapped'
import { use as i18n } from '--/store/i18n'

/**
 * Timestamp data component.
 *
 * @function                Timestamp
 * @param    {Object|Model} timestamp Timestamp to display
 */
export default function Details (props) {
  // Load internationalization context
  const [tr] = i18n()

  return (
    <StatusWrapped {...props}>
      <h3 class='text-xl mb-3 py-1 px-3 truncate border border-transparent'>
        {props.timestamp.title}
      </h3>

      <p class='py-1 px-3 border border-transparent break-words' classList={{ 'text-gray-400': !props.timestamp.description }}>
        {props.timestamp.description || tr.timestamp.descriptionless}
      </p>
    </StatusWrapped>
  )
}
