// Package imports
import { Match, Show, Switch } from 'solid-js/dom'

// Application imports
import Details from './details'
import Header from './header'
import Update from './update'
import Persistence from './persistence'
import { use as data } from '--/store/data'

/**
 * Timestamp data component.
 *
 * @function          Timestamp
 * @param    {Object} props     Component properties
 * @returns  {JSX}
 */
export default function Timestamp (props) {
  // Get the model's component state manager from our data context
  const [, { state }] = data()

  // View state is not in the data context's state; it has its own state data
  const { views: { is: view } } = state(props.timestamp)

  return (
    <div class='my-3 border rounded'>
      <Header {...props} />

      <Show when={!view.closed()}>
        <div class='border-t p-3'>
          <Switch>
            <Match when={view.details()}>
              <Details {...props} />
            </Match>

            <Match when={view.local()}>
              <Persistence
                icon='sd_storage'
                name='local'
                {...props}
              />
            </Match>

            <Match when={view.remote()}>
              <Persistence
                icon='cloud'
                name='remote'
                {...props}
              />
            </Match>

            <Match when={view.update()}>
              <Update {...props} />
            </Match>
          </Switch>
        </div>
      </Show>
    </div>
  )
}
