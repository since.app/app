// Application imports
import StatusWrapped from './persistence/status-wrapped'
import { use as data } from '--/store/data'

/**
 * Update timestamp component.
 *
 * @function          Update
 * @param    {Object} props  Component properties
 * @returns  {JSX}
 */
export default function Update (props) {
  // Loading data context
  const [, { state }] = data()

  // Getting the current timestamp's state
  const { forms } = state(props.timestamp)

  // Getting the timestamp's update form
  const form = forms.update()

  // Update form onSubmit event handler
  const onSubmit = (e) => { e.preventDefault() }

  return (
    <StatusWrapped timestamp={props.timestamp} tooltip={false}>
      {form.render({ onSubmit })}
    </StatusWrapped>
  )
}
