// Package imports
import { Show } from 'solid-js/dom'

// Application imports
import Menu from './menu'
import Stopwatch from '../stopwatch'
import { use as i18n } from '--/store/i18n'
import { use as data } from '--/store/data'

export default function Header (props) {
  const [, { format }] = i18n()
  const [, { state }] = data()

  const { views: { is: view } } = state(props.timestamp)

  return (
    <div class='flex items-center p-3 bg-gray-100 text-xl'>
      <Show when={view.closed()}>
        <div class='flex-1 min-w-0 truncate'>
          <span class='font-mono tracking-widest font-bold px-3'>
            <Stopwatch since={props.timestamp.date} />
          </span>

          {props.timestamp.title}
        </div>
      </Show>

      <Show when={!view.closed()}>
        <div class='flex-1 pl-3'>
          {format(props.timestamp.date)}
        </div>
      </Show>

      <div class='flex-shrink'>
        <Menu {...props} />
      </div>
    </div>
  )
}
