// Application imports
import Status from './status'
import { use as data } from '--/store/data'

export default function StatusWrapped (props) {
  // Access our data store
  const [, { state }] = data()

  const { events, views: { is: view } } = state(props.timestamp)
  const { onLocal, onRemote } = events

  return (
    <div class='flex'>
      <div class='flex-1 min-w-0 pr-3'>
        {props.children}
      </div>

      <div class='flex-shrink'>
        <Status
          icon='cloud'
          name='remote'
          tooltip={props.tooltip !== false && !view.remote()}
          onClick={() => props.tooltip !== false && onRemote()}
          {...props}
        />

        <Status
          icon='sd_storage'
          name='local'
          tooltip={props.tooltip !== false && !view.local()}
          onClick={() => props.tooltip !== false && onLocal()}
          {...props}
        />
      </div>
    </div>
  )
}
