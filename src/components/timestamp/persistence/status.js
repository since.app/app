// Package imports
import { Show } from 'solid-js/dom'

// Application imports
import IconStack from './icon-stack'
import { use as data } from '--/store/data'
import { use as i18n } from '--/store/i18n'
import { translateStatus } from './index'

/**
 * Persistence status idicators.
 *
 * @function            Status
 * @param    {Object}   props      Component properties
 * @param    {String}   props.icon Icon to display
 * @param    {String}   props.name Persistence name
 * @param    {Function} props.sync Sync status read signal
 * @returns  {JSX}
 */
export default function Status (props) {
  // Loading data and internationalization context
  const [store] = data()
  const [tr] = i18n()

  // Sync status
  const status = () => store.persists[props.name][props.timestamp.id]

  return (
    <div class='relative'>
      <button
        type='button'
        class='ui.btn ui.btn-blank ui.tooltip-target relative z-30'
        classList={{ 'cursor-default': props.tooltip === false }}
        onClick={props.onClick}
      >
        <IconStack icon={props.icon} status={status()} />
      </button>

      <Show when={props.tooltip !== false}>
        <div class='ui.tooltip text-gray-700 z-20'>
          {tr.persist[props.name].name} {translateStatus(status(), tr.persist[props.name])}
        </div>
      </Show>
    </div>
  )
}
