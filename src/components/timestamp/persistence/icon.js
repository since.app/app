/**
 * Material design icon component.
 *
 * @function          Icon
 * @param    {Object} props       Component properties
 * @param    {String} props.class Additional CSS classes
 * @returns  {JSX}
 */
export default function Icon (props) {
  return (
    <i class={'material-icons ' + (props.class || '')}>
      {props.icon}
    </i>
  )
}
