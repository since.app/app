// Package imports
import { Show } from 'solid-js/dom'

// Application imports
import { capitalize, is } from '--/utils'
import { use as data } from '--/store/data'
import { use as i18n } from '--/store/i18n'

import Fixable from './fixable'
import StatusWrapped from './status-wrapped'

export function translateStatus (message, tr) {
  switch (message) {
    case true:
      // True means successfully synced
      return tr.synced
    case false:
      // False means sync in progress
      return tr.syncing
    case undefined:
      // Undefined means we have no connection
      return tr.offline
    default:
      // Anything else is considered an error message
      return tr.error
  }
}

export default function Persistence (props) {
  // Loading our data and internationalization contexts
  const [store] = data()
  const [tr] = i18n()

  // Reactive persistence status getter
  const status = () => store.persists[props.name][props.timestamp.id]

  // Reactive colour class setter
  const statusColor = () => {
    const type = typeof status()
    return {
      'text-blue-600': type === 'boolean',
      'text-gray-600': type === 'undefined',
      'text-red-600': type === 'string'
    }
  }

  return (
    <StatusWrapped timestamp={props.timestamp}>
      <div class='flex'>
        <div class='flex-shrink px-3'>
          <i class='material-icons m-px relative top-1/2 transform -translate-y-1/2'>{props.icon}</i>
        </div>

        <h3 class='flex-1 text-xl'>
          {capitalize(props.name)} Persistence
        </h3>
      </div>

      <dl class='p-3'>
        <dt class='font-bold'>Driver:</dt>
        <dd class='px-3'>
          {capitalize(tr.persist[props.name].name)}
        </dd>

        <dt class='font-bold'>Status:</dt>
        <dd class='px-3'>
          <strong classList={statusColor()}>
            {capitalize(translateStatus(status(), tr.persist[props.name]))}
          </strong>

          <Fixable status={status()} timestamp={props.timestamp} />
        </dd>

        <Show when={is.str(status())}>
          <dt class='font-bold'>Message:</dt>
          <dd class='px-3'>{status()}</dd>
        </Show>
      </dl>
    </StatusWrapped>
  )
}
