import { Show } from 'solid-js/dom'

import { use as data } from '--/store/data'
import { use as i18n } from '--/store/i18n'
import { is, stopwatch } from '--/utils'
import { UUID } from '--/utils/is'

// Helper function that generates data for a "missing" timestamp
const missing = () => ({ date: Date.now() - 1, title: 'ERROR FINDING TIMESTAMP IN STATE DATA' })

export default function Fixable (props) {
  const [store, { cloneTimestamp, deleteTimestamp }] = data()
  const [, { format }] = i18n()

  // Helper reducer that returns the last UUIDv4 found in an array of strings
  const uuid = (a, v) => v.match(UUID) ? v.match(UUID)[0] : a

  // Helper function that plucks the UUID from a given message
  const target = (status) => String(status).split(' ').reduce(uuid)

  // Helper function that checks if the current timestamp UUID is in the error status
  const source = () => target(props.status) === props.timestamp.id

  // Fix is usually to simply generate a new UUID
  const onSource = () => {
    // Clone creates a carbon copy of the timestamp
    cloneTimestamp(props.timestamp)

    // Delete the conflicting timestamp in our states
    deleteTimestamp(props.timestamp)
  }

  // Displays information about the conflicting timestamp
  const onOther = () => {
    const timestamp = store.timestamps[target(props.status)] || missing()
    const offset = stopwatch(Date.now() - timestamp.date)

    // Generating an alert message for now
    let message = 'Conflicting timestamp date and title:'

    message += `\n  ${format(timestamp.date)}`
    message += `\n    (${offset.d} days, ${offset.h} hours, ${offset.i} minutes ago)`
    message += `\n  ${timestamp.title}`

    window.alert(message)
  }

  return (
    <Show when={is.str(props.status)}>
      <button onClick={source() ? onSource : onOther} type='button' class='ml-3'>
        ( <strong class='underline text-blue-600'>{source() ? 'fix' : 'see other'}</strong> )
      </button>
    </Show>
  )
}
