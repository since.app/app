// Package imports
import { Switch, Match } from 'solid-js/dom'

// Application imports
import Icon from './icon'
import { is } from '--/utils'

/**
 * Displays reactive statuses for a given icon.
 *
 * @function            IconStack
 * @param    {Object}   props      Component properties
 * @param    {String}   props.icon Base icon to display
 * @param    {Function} props.sync Sync status read signal
 * @returns  {JSX}
 */
export default function IconStack (props) {
  return (
    <div class='relative'>
      <Icon icon={props.icon} class='text-gray-400' />

      <div class='absolute mt-1 -mr-1 top-0 right-0 z-10'>
        <Switch>
          <Match when={is.true(props.status)}>
            <Icon icon='done' class='text-blue-600' />
          </Match>

          <Match when={is.false(props.status)}>
            <Icon icon='autorenew' class='text-blue-600 animate-spin' />
          </Match>

          <Match when={is.undef(props.status)}>
            <Icon icon='not_interested' class='text-gray-600' />
          </Match>

          <Match when={is.str(props.status)}>
            <Icon icon='error_outline' class='text-red-600' />
          </Match>
        </Switch>
      </div>
    </div>
  )
}
